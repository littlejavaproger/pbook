package com.getjavajob.nesterenkom.pbook.model;

public class User extends BaseEntity {

    private String login;
    private String password;
    private ListRole userRole;

    public User() {
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public User(String login, String password, ListRole userRole) {
        this.login = login;
        this.password = password;
        this.userRole = userRole;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ListRole getUserRole() {
        return userRole;
    }

    public void setUserRole(ListRole userRole) {
        this.userRole = userRole;
    }

    @Override
    public String toString() {
        return login;
    }
}
