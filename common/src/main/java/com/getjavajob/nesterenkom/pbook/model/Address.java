package com.getjavajob.nesterenkom.pbook.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "address_tbl")
public class Address extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 7220962078326800565L;

    @Column(name = "address", nullable = false, length = 100)
    private String address;
    @Column(name = "type_address", nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeAddress type;

    public Address() {
    }

    public Address(int id) {
        super(id);
    }

    public Address(String address) {
        this.address = address;
    }

    public Address(int id, String address) {
        super(id);
        this.address = address;
    }

    public Address(String address, TypeAddress type) {
        this.address = address;
        this.type = type;
    }

    public Address(int id, String address, TypeAddress type) {
        super(id);
        this.address = address;
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public TypeAddress getType() {
        return type;
    }

    public void setType(TypeAddress type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address1 = (Address) o;

        if (address != null ? !address.equals(address1.address) : address1.address != null) return false;
        return type == address1.type;

    }

    @Override
    public int hashCode() {
        int result = address != null ? address.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Address = " + address + ' ' + type;
    }
}
