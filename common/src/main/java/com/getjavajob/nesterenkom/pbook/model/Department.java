package com.getjavajob.nesterenkom.pbook.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "department_tbl", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name_department")
})
public class Department extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 2891035121478916255L;

    @Column(name = "name_department", unique = false, length = 40)
    private String name;
    @OneToOne
    @JoinColumn(name = "id_head_department")
    private Employee headDepartment;

    public Department() {
    }

    public Department(int id) {
        super(id);
    }

    public Department(String name, Employee headDepartment) {
        this.name = name;
        this.headDepartment = headDepartment;
    }

    public Department(int id, String name, Employee headDepartment) {
        super(id);
        this.name = name;
        this.headDepartment = headDepartment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getHeadDepartment() {
        return headDepartment;
    }

    public void setHeadDepartment(Employee headDepartment) {
        this.headDepartment = headDepartment;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return !(headDepartment != null ? !headDepartment.equals(that.headDepartment) : that.headDepartment != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (headDepartment != null ? headDepartment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Department{ " + name + '\n' +
                "HeadDepartment = " + (headDepartment == null ? null : headDepartment.getId()) + '\n' +
                '}';
    }
}
