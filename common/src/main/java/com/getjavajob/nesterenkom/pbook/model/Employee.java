package com.getjavajob.nesterenkom.pbook.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "employee_tbl", uniqueConstraints = {
        @UniqueConstraint(columnNames = "email"),
        @UniqueConstraint(columnNames = "icq"),
        @UniqueConstraint(columnNames = "skype"),
})
public class Employee extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 1292572750279451391L;

    @Column(name = "surname", nullable = false, length = 20)
    private String surname;
    @Column(name = "first_name", nullable = false, length = 15)
    private String name;
    @Column(name = "family_name", nullable = false, length = 25)
    private String middleName;
    @Column(name = "birthday", nullable = false)
    @Temporal(value = TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private Date birthday;
    @OneToMany
            (cascade = CascadeType.ALL)
    @JoinTable(name = "telephone_employee_tbl", joinColumns = {@JoinColumn(name = "id_employee")}, inverseJoinColumns = {
            @JoinColumn(name = "id_telephone")})
    private List<Telephone> telephones;
    @ManyToMany
            (cascade = CascadeType.ALL)
    @JoinTable(name = "address_employee_tbl", joinColumns = {@JoinColumn( name = "id_employee")}, inverseJoinColumns = {
            @JoinColumn(name = "id_address")
    })
    private List<Address> addresses;
    @Column(name = "email",unique = true, length = 20)
    private String email;
    @Column(name = "icq",unique = true, length = 20)
    private String icq;
    @Column(name = "skype",unique = true, length = 20)
    private String skype;
    @ManyToOne
    @JoinColumn(name = "id_leader")
    private Employee leader;
    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;
    @Column(name = "photo", nullable = true)
    @Lob
    private Blob photo;

    public Employee() {
    }

    public Employee(int id) {
        super(id);
    }

    public Employee(String surname, String name, String middleName, Date birthday, List<Telephone> telephones, List<Address> addresses, String email, String icq, String skype, Employee leader, Department department) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.birthday = birthday;
        this.telephones = telephones;
        this.addresses = addresses;
        this.email = email;
        this.icq = icq;
        this.skype = skype;
        this.leader = leader;
        this.department = department;
    }

    public Employee(String surname, String name, String middleName, Date birthday, List<Telephone> telephones, List<Address> addresses, String email, String icq, String skype, Employee leader, Department department, Blob photo) {
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.birthday = birthday;
        this.telephones = telephones;
        this.addresses = addresses;
        this.email = email;
        this.icq = icq;
        this.skype = skype;
        this.leader = leader;
        this.department = department;
        this.photo = photo;
    }

    public Employee(int id, String surname, String name, String middleName, Date birthday, List<Telephone> telephones, List<Address> addresses, String email, String icq, String skype, Employee leader, Department department) {
        super(id);
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.birthday = birthday;
        this.telephones = telephones;
        this.addresses = addresses;
        this.email = email;
        this.icq = icq;
        this.skype = skype;
        this.leader = leader;
        this.department = department;
    }

    public Employee(int id, String surname, String name, String middleName, Date birthday, List<Telephone> telephones, List<Address> addresses, String email, String icq, String skype, Employee leader, Department department, Blob photo) {
        super(id);
        this.surname = surname;
        this.name = name;
        this.middleName = middleName;
        this.birthday = birthday;
        this.telephones = telephones;
        this.addresses = addresses;
        this.email = email;
        this.icq = icq;
        this.skype = skype;
        this.leader = leader;
        this.department = department;
        this.photo = photo;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Telephone> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<Telephone> telephones) {
        this.telephones = telephones;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public Employee getLeader() {
        return leader;
    }

    public void setLeader(Employee leader) {
        this.leader = leader;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @JsonIgnore
    public Blob getPhoto() {
        return photo;
    }

    public void setPhoto(Blob photo) {
        this.photo = photo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        if (surname != null ? !surname.equals(employee.surname) : employee.surname != null) return false;
        if (name != null ? !name.equals(employee.name) : employee.name != null) return false;
        if (middleName != null ? !middleName.equals(employee.middleName) : employee.middleName != null) return false;
        if (birthday != null ? !birthday.equals(employee.birthday) : employee.birthday != null) return false;
        if (telephones != null ? !telephones.equals(employee.telephones) : employee.telephones != null) return false;
        if (addresses != null ? !addresses.equals(employee.addresses) : employee.addresses != null) return false;
        if (email != null ? !email.equals(employee.email) : employee.email != null) return false;
        if (icq != null ? !icq.equals(employee.icq) : employee.icq != null) return false;
        if (skype != null ? !skype.equals(employee.skype) : employee.skype != null) return false;
        if (leader != null ? !leader.equals(employee.leader) : employee.leader != null) return false;
        return !(department != null ? !department.equals(employee.department) : employee.department != null);

    }

    @Override
    public int hashCode() {
        int result = 31 * (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        result = 31 * result + (telephones != null ? telephones.hashCode() : 0);
        result = 31 * result + (addresses != null ? addresses.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (icq != null ? icq.hashCode() : 0);
        result = 31 * result + (skype != null ? skype.hashCode() : 0);
        result = 31 * result + (leader != null ? leader.hashCode() : 0);
        result = 31 * result + (department != null ? department.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        SimpleDateFormat simpleDate = new SimpleDateFormat("MM/dd/yyyy");

        return "Employee{" +
                "id=" + id +
                ", firstName='" + surname + '\'' +
                ", lastName='" + name + '\'' +
                ", middleName='" + middleName + '\'' +
                ", dateOfBirth=" + simpleDate.format(birthday) +
                ", phones=" + telephones +
                ", address='" + addresses + '\'' +
                ", eMail='" + email + '\'' +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", department=" + department +
                ", chiefId=" + (leader == null ? null : leader.getId()) +
                '}';
    }
}
