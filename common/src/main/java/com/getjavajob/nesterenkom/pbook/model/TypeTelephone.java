package com.getjavajob.nesterenkom.pbook.model;

public enum TypeTelephone {
   MOBILE, DEPARTMENT, HOME, WORK
}
