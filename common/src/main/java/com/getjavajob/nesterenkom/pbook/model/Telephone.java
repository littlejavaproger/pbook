package com.getjavajob.nesterenkom.pbook.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "telephone_tbl", uniqueConstraints = {
        @UniqueConstraint(columnNames = "telephone_number")
})
public class Telephone extends BaseEntity implements Serializable{

    private static final long serialVersionUID = -2057271018822770140L;

    @Column(name = "telephone_number", nullable = false, unique = true, length = 18)
    private String number;
    @Column(name = "type_telephone", nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeTelephone type;

    public Telephone() {
    }

    public Telephone(String number, TypeTelephone type) {
        this.number = number;
        this.type = type;
    }

    public Telephone(int id, String number, TypeTelephone type) {
        super(id);
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public TypeTelephone getType() {
        return type;
    }

    public void setType(TypeTelephone type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Telephone telephone = (Telephone) o;

        if (!number.equals(telephone.number)) return false;
        return type == telephone.type;

    }

    @Override
    public int hashCode() {
        int result = number.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Telephone{ " + type + ' ' + "number = " + number + '\n' + '}';
    }
}
