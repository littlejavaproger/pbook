package com.getjavajob.nesterenkom.pbook.test;

import com.getjavajob.nesterenkom.pbook.dao.pool.ConnectionPool;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class ConnectionPoolTest {

    @Test
    public void getConnectionTest() throws SQLException {
        ConnectionPool pool = ConnectionPool.getInstance();

        try(Connection firstConnection = pool.getConnection()){
            try(Connection secondConnection = pool.getConnection()){
                Assert.assertTrue(firstConnection.equals(secondConnection));
            }
        }
    }
}
