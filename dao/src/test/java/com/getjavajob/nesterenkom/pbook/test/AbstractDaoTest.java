package com.getjavajob.nesterenkom.pbook.test;

import com.getjavajob.nesterenkom.pbook.dao.pool.ConnectionPool;
import com.getjavajob.nesterenkom.pbook.model.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractDaoTest {

    private static boolean isInitialize;

    @BeforeClass
    public static void onBeforeClass() throws IOException, SQLException {
        if(!isInitialize){
            initialize();
            isInitialize = true;
        }
    }

    @Before
    public void onBefore() throws IOException, SQLException {
        loadData();
    }

    @Test
    public abstract void add();

    @Test
    public abstract void update();

    @Test
    public abstract void get();

    @Test
    public abstract void delete();

    @Test
    public abstract void getAll();

    protected List<Employee> getTestEmployees(){
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        List<Employee> employees = new ArrayList<>();

        //EMPLOYEE #1
        Employee firstEmployee = new Employee();
        firstEmployee.setId(1);
        firstEmployee.setSurname("IVANOV");
        firstEmployee.setName("IVAN");
        firstEmployee.setMiddleName("IVANOVICH");
        try {
            firstEmployee.setBirthday(simpleDate.parse("1991-05-02"));
        } catch (ParseException ignored) {
        }
        //Telephones Emp1
        List<Telephone> telephones = new ArrayList<>();
        telephones.add(new Telephone("1-1", TypeTelephone.WORK));
        telephones.add(new Telephone("1-2", TypeTelephone.HOME));
        firstEmployee.setTelephones(telephones);
        //Address Emp1
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("HOME_ADDRESS_EMPLOYEE_1"));
        addresses.add(new Address("WORK_ADDRESS_EMPLOYEE_1"));
        firstEmployee.setAddresses(addresses);
        firstEmployee.setEmail("IVANOV@MAIL.RU");
        firstEmployee.setIcq("1234");
        firstEmployee.setSkype("IVANOV_SKYPE");
        firstEmployee.setLeader(null);
        //Department #1
        Department department = new Department();
        department.setId(1);
        department.setName("HEAD_DEP");
        department.setHeadDepartment(null);
        firstEmployee.setDepartment(department);

        employees.add(firstEmployee);
        //EMPLOYEE #2
        Employee secondEmployee = new Employee();
        secondEmployee.setId(2);
        secondEmployee.setSurname("PETROV");
        secondEmployee.setName("PETR");
        secondEmployee.setMiddleName("PETROVICH");
        try {
            secondEmployee.setBirthday(simpleDate.parse("1945-07-11"));
        } catch (ParseException ignored) {
        }
        //Telephones Emp2
        telephones = new LinkedList<>();
        telephones.add(new Telephone("2-1", TypeTelephone.WORK));
        telephones.add(new Telephone("2-2", TypeTelephone.HOME));
        secondEmployee.setTelephones(telephones);
        //Address Emp2
        addresses = new ArrayList<>();
        addresses.add(new Address("HOME_ADDRESS_EMPLOYEE_2"));
        addresses.add(new Address("WORK_ADDRESS_EMPLOYEE_2"));
        secondEmployee.setAddresses(addresses);
        secondEmployee.setEmail("PETROV@MAIL.RU");
        secondEmployee.setIcq("4321");
        secondEmployee.setSkype("PETROV_SKYPE");
        secondEmployee.setLeader(null);
        //Department #2
        department = new Department();
        department.setId(2);
        department.setName("PR_DEP");
        department.setHeadDepartment(null);
        secondEmployee.setDepartment(department);

        employees.add(secondEmployee);

        return employees;
    }

    protected List<Department> getTestDepartments(){
        List<Department> departments = new ArrayList<>();

        Department firstDepartment = new Department();
        firstDepartment.setId(1);
        firstDepartment.setName("HEAD_DEP");
        firstDepartment.setHeadDepartment(null);

        departments.add(firstDepartment);

        Department secondDepartment = new Department();
        secondDepartment.setId(2);
        secondDepartment.setName("PR_DEP");
        secondDepartment.setHeadDepartment(null);

        departments.add(secondDepartment);

        return departments;
    }

    private static void initialize() throws SQLException, IOException {
        executeSqlFile("create-data-model.sql");
    }

    private static void loadData() throws IOException, SQLException {
        executeSqlFile("data-database.sql");
    }

    private static void executeSqlFile(String fileName) throws SQLException, IOException {
        try(Connection connection = ConnectionPool.getInstance().getConnection();
            Statement statement  = connection.createStatement())
        {
            InputStreamReader reader = new InputStreamReader(ClassLoader.getSystemResourceAsStream(fileName));
            BufferedReader bufferedReader = new BufferedReader(reader);
            StringBuilder stringBuilder = new StringBuilder();
            while (bufferedReader.ready()){
                String sqlLine = bufferedReader.readLine();
                stringBuilder.append(sqlLine).append("\n");
                if (sqlLine.contains(";")){
                    statement.execute(stringBuilder.toString());
                    stringBuilder = new StringBuilder();
                }
            }
        }
    }
}
