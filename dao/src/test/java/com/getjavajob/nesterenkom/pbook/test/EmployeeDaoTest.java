package com.getjavajob.nesterenkom.pbook.test;

import com.getjavajob.nesterenkom.pbook.dao.impl.EmployeeDao;
import com.getjavajob.nesterenkom.pbook.model.*;
import org.junit.Assert;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoTest extends AbstractDaoTest {

    private EmployeeDao employeeDao = new EmployeeDao();
    private SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public void add() {
        Employee employeeForAdd = new Employee();
        employeeForAdd.setSurname("Nesterenko");
        employeeForAdd.setName("Maxim");
        employeeForAdd.setMiddleName("Sergeevich");
        try {
            employeeForAdd.setBirthday(simpleDate.parse("1991-01-02"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Telephone> telephones = new ArrayList<>();
        telephones.add(new Telephone("123", TypeTelephone.WORK));
        telephones.add(new Telephone("+71234567889", TypeTelephone.HOME));
        employeeForAdd.setTelephones(telephones);
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("HOME"));
        addresses.add(new Address("WORK"));
        employeeForAdd.setAddresses(addresses);
        employeeForAdd.setEmail("Nesterenko@mail.com");
        employeeForAdd.setIcq("123456789");
        employeeForAdd.setSkype("nesterio222");
        employeeForAdd.setDepartment(getTestDepartments().get(0));
        employeeForAdd.setLeader(getTestEmployees().get(1));
        employeeDao.add(employeeForAdd);

        Assert.assertEquals(employeeForAdd, employeeDao.get(employeeForAdd.getId()));
    }

    @Override
    public void get() {
        List<Employee> testEmployees = getTestEmployees();

        Employee expectedFirstEmployee = testEmployees.get(0);
        Employee actualFirstEmployee = employeeDao.get(1);
        Assert.assertEquals(expectedFirstEmployee, actualFirstEmployee);

        Employee expectedSecondEmployee = testEmployees.get(1);
        Employee actualSecondEmployee = employeeDao.get(2);
        Assert.assertEquals(expectedSecondEmployee, actualSecondEmployee);
    }

    @Override
    public void delete() {
        Assert.assertNotNull(employeeDao.get(1));

        employeeDao.delete(1);
        Assert.assertNull(employeeDao.get(1));
    }

    @Override
    public void update() {
        Employee employee = employeeDao.get(1);
        System.out.println(employee);
        employee.setName("Sergey");
        employeeDao.update(employee);
        System.out.println(employee);

        Assert.assertEquals(employee, employeeDao.get(employee.getId()));
    }

    @Override
    public void getAll() {
        List<Employee> testEmployees = getTestEmployees();
        List<Employee> allEmployees = employeeDao.getAll();

        Assert.assertTrue(allEmployees.containsAll(testEmployees));
    }
}
