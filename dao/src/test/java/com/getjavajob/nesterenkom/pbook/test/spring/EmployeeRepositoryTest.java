package com.getjavajob.nesterenkom.pbook.test.spring;

import com.getjavajob.nesterenkom.pbook.model.*;
import com.getjavajob.nesterenkom.pbook.repository.impl.AddressRepositoryImpl;
import com.getjavajob.nesterenkom.pbook.repository.impl.DepartmentRepositoryImpl;
import com.getjavajob.nesterenkom.pbook.repository.impl.EmployeeRepositoryImpl;
import com.getjavajob.nesterenkom.pbook.repository.impl.TelephoneRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:pbook-context-dao.xml", "classpath:pbook-context-dao-overrides.xml"})
@Transactional
public class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepositoryImpl employeeRepository;

    @Autowired
    private DepartmentRepositoryImpl departmentRepository;

    @Autowired
    private TelephoneRepositoryImpl telephoneRepository;

    @Autowired
    private AddressRepositoryImpl addressRepository;

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Test
    public void getSurnameById(){
        Employee employee = employeeRepository.findById(1);

        assertEquals("SURNAME1", employee.getSurname());
    }

    @Test
    public void getNameById(){
        Employee employee = employeeRepository.findById(1);

        assertEquals("NAME1", employee.getName());
    }

    @Test
    public void getMiddleNameById(){
        Employee employee = employeeRepository.findById(1);

        assertEquals("MIDDLE_NAME1", employee.getMiddleName());
    }

    @Test
    public void getDateById() throws ParseException {
        Employee employee = employeeRepository.findById(1);


        assertEquals(simpleDateFormat.parse("1991-05-02"), employee.getBirthday());
    }

    @Test
    public void getEmailById(){
        Employee employee = employeeRepository.findById(1);

        assertEquals("EMPLOYEE1@MAIL.RU", employee.getEmail());
    }

    @Test
    public void getSkypeById(){
        Employee employee = employeeRepository.findById(1);

        assertEquals("EMPLOYEE1_SKYPE", employee.getSkype());
    }

    @Test
    public void getIcqById(){
        Employee employee = employeeRepository.findById(1);

        assertEquals("01", employee.getIcq());
    }

    @Test
    public void getTelephoneById(){
        Employee employee = employeeRepository.findById(1);
        List<Telephone> telephones = new ArrayList<>();
        telephones.add(new Telephone("1-1", TypeTelephone.WORK));
        telephones.add(new Telephone("1-2", TypeTelephone.HOME));

        assertEquals(telephones, employee.getTelephones());
    }

    @Test
    public void getAddressEmployeeById(){
        Employee employee = employeeRepository.findById(1);
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("HOME_ADDRESS_EMPLOYEE_1", TypeAddress.HOME));
        addresses.add(new Address("WORK_ADDRESS_EMPLOYEE_1", TypeAddress.WORK));

        assertEquals(addresses, employee.getAddresses());
    }

    @Test
    public void getLiderByid(){
        Employee employee = employeeRepository.findById(1);

        assertNull(employee.getLeader());
    }

    @Test
    public void getDepartmentById(){
        Employee employee = employeeRepository.findById(1);

        assertEquals("DEP1", employee.getDepartment().getName());
        assertNull(employee.getDepartment().getHeadDepartment());
    }

    @Test
    @Transactional
    public void addEmployee(){
        Employee employee = new Employee();
        employee.setId(3);
        employee.setSurname("SURNAME3");
        employee.setName("NAME3");
        employee.setMiddleName("MIDDLE_NAME3");
        try {
            employee.setBirthday(simpleDateFormat.parse("1991-05-02"));
        } catch (ParseException ignored) {
        }
        //Telephones Emp1
        List<Telephone> telephones = new ArrayList<>();
        telephones.add(new Telephone("3-1", TypeTelephone.WORK));
        telephones.add(new Telephone("3-2", TypeTelephone.HOME));
        employee.setTelephones(telephones);
        //Address Emp1
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("HOME_ADDRESS_EMPLOYEE_3", TypeAddress.HOME));
        addresses.add(new Address("WORK_ADDRESS_EMPLOYEE_3", TypeAddress.WORK));
        employee.setAddresses(addresses);
        employee.setEmail("EMPLOYEE3@MAIL.RU");
        employee.setIcq("03");
        employee.setSkype("EMPLOYEE3_SKYPE");
        employee.setLeader(null);
        employee.setDepartment(departmentRepository.findById(1));

        employeeRepository.add(employee);
        assertTrue(employeeRepository.findById(3).getId() > 0);
        assertEquals("SURNAME3", employeeRepository.findById(3).getSurname());
    }

    @Test
    @Transactional
    public void editEmployee(){
        Employee employee = employeeRepository.findById(2);
        employee.setSurname("SURNAME4");
        employeeRepository.update(employee);

        assertEquals("SURNAME4", employeeRepository.findById(2).getSurname());
    }

    @Test
    @Transactional
    public void removeEmployee(){
        employeeRepository.delete(2);

        assertNull(employeeRepository.findById(2));
    }

    @Test
    public void getAllEmployee() throws ParseException {
        List<Employee> employeesActual = employeeRepository.findAll();
        List<Employee> employeesExpected = new ArrayList<>();


        //Telephones Emp1
        List<Telephone> telephones1 = new ArrayList<>();
        telephones1.add(new Telephone("1-1", TypeTelephone.WORK));
        telephones1.add(new Telephone("1-2", TypeTelephone.HOME));
        //Address Emp1
        List<Address> addresses1 = new ArrayList<>();
        addresses1.add(new Address("HOME_ADDRESS_EMPLOYEE_1", TypeAddress.HOME));
        addresses1.add(new Address("WORK_ADDRESS_EMPLOYEE_1", TypeAddress.WORK));

        //Telephones Emp2
        List<Telephone> telephones2 = new ArrayList<>();
        telephones2.add(new Telephone("2-1", TypeTelephone.WORK));
        telephones2.add(new Telephone("2-2", TypeTelephone.HOME));
        //Address Emp2
        List<Address> addresses2 = new ArrayList<>();
        addresses2.add(new Address("HOME_ADDRESS_EMPLOYEE_2", TypeAddress.HOME));
        addresses2.add(new Address("WORK_ADDRESS_EMPLOYEE_2", TypeAddress.WORK));

        Employee employee1 = new Employee(1, "SURNAME1", "NAME1", "MIDDLE_NAME1", simpleDateFormat.parse("1991-05-02"), telephones1, addresses1, "EMPLOYEE1@MAIL.RU", "01", "EMPLOYEE1_SKYPE", null, departmentRepository.findById(1));
        Employee employee2 = new Employee(2, "SURNAME2", "NAME2", "MIDDLE_NAME2", simpleDateFormat.parse("1945-07-11"), telephones2, addresses2, "EMPLOYEE2@MAIL.RU", "02", "EMPLOYEE2_SKYPE", employeeRepository.findById(1), departmentRepository.findById(2));
        employeesExpected.add(employee1);
        employeesExpected.add(employee2);

        assertEquals(employeesExpected, employeesActual);
    }

    @Test
    public void getDepartmentNameById(){
        assertEquals("DEP1", departmentRepository.findById(1).getName());
    }

    @Test
    public void getDepartmentHeadById(){
        assertNull(departmentRepository.findById(1).getHeadDepartment());
    }

    @Test
    public void getAllDepartment(){
        Employee employee = new Employee();
        List<Department> departmentActual = departmentRepository.findAll();
        List<Department> departmentExpected = new ArrayList<>();
        departmentExpected.add(new Department(1, "DEP1", employeeRepository.findById(3)));
        departmentExpected.add(new Department(2, "DEP2", employeeRepository.findById(3)));

        assertEquals(departmentExpected, departmentActual);
    }

    @Test
    @Transactional
    public void addDepartment(){
        Department department = new Department(3, "DEP3", employeeRepository.findById(3));
        departmentRepository.add(department);

        assertEquals("DEP3", departmentRepository.findById(3).getName());
        assertNotNull(departmentRepository.findById(3));
    }

    @Test
    @Transactional
    public void removeDepartment(){
        departmentRepository.delete(2);

        assertNull(departmentRepository.findById(2));
    }

    @Test
    @Transactional
    public void editDepartment(){
        Department department = departmentRepository.findById(2);
        department.setName("DEP5");

        assertEquals("DEP5", departmentRepository.findById(2).getName());
    }

    @Test
    public void getTelephoneNumberById(){
        assertEquals("1-1", telephoneRepository.findById(1).getNumber());
        assertEquals("1-2", telephoneRepository.findById(2).getNumber());
        assertEquals("2-1", telephoneRepository.findById(3).getNumber());
        assertEquals("2-2", telephoneRepository.findById(4).getNumber());
    }

    @Test
    public void getTelephoneTypeById(){
        assertEquals(TypeTelephone.WORK, telephoneRepository.findById(1).getType());
        assertEquals(TypeTelephone.HOME, telephoneRepository.findById(2).getType());
        assertEquals(TypeTelephone.WORK, telephoneRepository.findById(3).getType());
        assertEquals(TypeTelephone.HOME, telephoneRepository.findById(4).getType());
    }

    @Test
    public void getAllTelephone(){
        List<Telephone> telephones = new ArrayList<>();
        telephones.add(new Telephone("1-1", TypeTelephone.WORK));
        telephones.add(new Telephone("1-2", TypeTelephone.HOME));
        telephones.add(new Telephone("2-1", TypeTelephone.WORK));
        telephones.add(new Telephone("2-2", TypeTelephone.HOME));

        assertEquals(telephones, telephoneRepository.findAll());
    }

    @Test
    @Transactional
    public void addTelephone(){
        Telephone telephone = new Telephone(5, "3-3", TypeTelephone.MOBILE);
        telephoneRepository.add(telephone);

        assertEquals("3-3", telephoneRepository.findById(5).getNumber());
        assertEquals(TypeTelephone.MOBILE, telephoneRepository.findById(5).getType());
    }

    @Test
    @Transactional
    public void removeTelephone(){
        telephoneRepository.delete(2);

        assertNull(telephoneRepository.findById(2));
    }

    @Test
    @Transactional
    public void editTelephoneNumber(){
        Telephone telephone = telephoneRepository.findById(2);
        telephone.setNumber("10-10");
        telephoneRepository.update(telephone);

        assertEquals("10-10", telephoneRepository.findById(2).getNumber());
    }


    @Test
    @Transactional
    public void editTelephoneType(){
        Telephone telephone = telephoneRepository.findById(2);
        telephone.setType(TypeTelephone.DEPARTMENT);
        telephoneRepository.update(telephone);

        assertEquals(TypeTelephone.DEPARTMENT, telephoneRepository.findById(2).getType());
    }

    @Test
    public void getAddressById(){
        assertEquals("HOME_ADDRESS_EMPLOYEE_1", addressRepository.findById(1).getAddress());
        assertEquals("WORK_ADDRESS_EMPLOYEE_1", addressRepository.findById(2).getAddress());
        assertEquals("HOME_ADDRESS_EMPLOYEE_2", addressRepository.findById(3).getAddress());
        assertEquals("WORK_ADDRESS_EMPLOYEE_2", addressRepository.findById(4).getAddress());
    }

    @Test
    public void getAddressTypeById(){
        assertEquals(TypeAddress.HOME, addressRepository.findById(1).getType());
        assertEquals(TypeAddress.WORK, addressRepository.findById(2).getType());
        assertEquals(TypeAddress.HOME, addressRepository.findById(3).getType());
        assertEquals(TypeAddress.WORK, addressRepository.findById(4).getType());
    }

    @Test
    public void getAllAddress(){
        List<Address> addresses = new ArrayList<>();
        addresses.add(new Address("HOME_ADDRESS_EMPLOYEE_1", TypeAddress.HOME));
        addresses.add(new Address("WORK_ADDRESS_EMPLOYEE_1", TypeAddress.WORK));
        addresses.add(new Address("HOME_ADDRESS_EMPLOYEE_2", TypeAddress.HOME));
        addresses.add(new Address("WORK_ADDRESS_EMPLOYEE_2", TypeAddress.WORK));

        assertEquals(1, addressRepository.findById(1).getId());
        assertEquals(2, addressRepository.findById(2).getId());
        assertEquals(3, addressRepository.findById(3).getId());
        assertEquals(4, addressRepository.findById(4).getId());
        assertNull(addressRepository.findById(5));
        assertEquals(addresses, addressRepository.findAll());
    }

    @Test
    @Transactional
    public void addAddress(){
        Address address = new Address(5, "Moscow", TypeAddress.HOME);
        addressRepository.add(address);

        assertNotNull(addressRepository.findById(5));
        assertEquals("Moscow", addressRepository.findById(5).getAddress());
    }

    @Test
    @Transactional
    public void removeAddress(){
        addressRepository.delete(2);

        assertNull(addressRepository.findById(2));
    }

    @Test
    @Transactional
    public void editAddress(){
        Address address = addressRepository.findById(2);
        address.setAddress("Saratov");
        addressRepository.update(address);

        assertEquals("Saratov", addressRepository.findById(2).getAddress());
    }

    @Test
    @Transactional
    public void editAddressType(){
        Address address = addressRepository.findById(2);
        address.setType(TypeAddress.HOME);
        addressRepository.update(address);

        assertEquals(TypeAddress.HOME, addressRepository.findById(2).getType());
    }
}
