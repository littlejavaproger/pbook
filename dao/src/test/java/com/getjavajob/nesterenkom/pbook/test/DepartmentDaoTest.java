package com.getjavajob.nesterenkom.pbook.test;

import com.getjavajob.nesterenkom.pbook.dao.impl.DepartmentDao;
import com.getjavajob.nesterenkom.pbook.dao.impl.EmployeeDao;
import com.getjavajob.nesterenkom.pbook.model.Department;
import org.junit.Assert;

import java.util.List;

public class DepartmentDaoTest extends AbstractDaoTest{
    DepartmentDao departmentDao = new DepartmentDao();

    @Override
    public void add() {
        Department department = new Department();
        department.setName("TEST_DEP");
        departmentDao.add(department);

        Assert.assertEquals(department, departmentDao.get(department.getId()));
    }

    @Override
    public void update() {
        Department department = departmentDao.get(1);
        department.setName("DEP_TEST");
        departmentDao.update(department);

        Assert.assertEquals(department, departmentDao.get(department.getId()));
    }

    @Override
    public void get() {
        List<Department> testDepartments = getTestDepartments();

        Department expectedFirstDepartment = testDepartments.get(0);
        Department actualFirstDepartment = departmentDao.get(1);
        Assert.assertEquals(expectedFirstDepartment, actualFirstDepartment);

        Department expectedSecondDepartment = testDepartments.get(1);
        Department actualSecondDepartment = departmentDao.get(2);
        Assert.assertEquals(expectedSecondDepartment, actualSecondDepartment);
    }

    @Override
    public void delete() {
        Assert.assertNotNull(departmentDao.get(1));

        //cause of FK it's need to delete employees before
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.delete(1);
        employeeDao.delete(2);

        departmentDao.delete(1);
        Assert.assertNull(departmentDao.get(1));
    }

    @Override
    public void getAll() {
        List<Department> testDepartments = getTestDepartments();
        List<Department> allDepartments = departmentDao.getAll();

        Assert.assertTrue(allDepartments.containsAll(testDepartments));
    }
}
