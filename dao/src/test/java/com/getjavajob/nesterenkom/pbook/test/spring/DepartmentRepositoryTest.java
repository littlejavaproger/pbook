package com.getjavajob.nesterenkom.pbook.test.spring;

import com.getjavajob.nesterenkom.pbook.model.*;
import com.getjavajob.nesterenkom.pbook.repository.impl.AddressRepositoryImpl;
import com.getjavajob.nesterenkom.pbook.repository.impl.DepartmentRepositoryImpl;
import com.getjavajob.nesterenkom.pbook.repository.impl.EmployeeRepositoryImpl;
import com.getjavajob.nesterenkom.pbook.repository.impl.TelephoneRepositoryImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:pbook-context-dao.xml", "classpath:pbook-context-dao-overrides.xml"})
@Transactional
public class DepartmentRepositoryTest {

    @Autowired
    private EmployeeRepositoryImpl employeeRepository;

    @Autowired
    private DepartmentRepositoryImpl departmentRepository;

    @Test
    public void getDepartmentName(){
        Department department = departmentRepository.findById(1);

        assertEquals("DEP1", department.getName());
    }


}
