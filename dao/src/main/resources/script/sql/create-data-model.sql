USE phonebook ;

CREATE TABLE employee_tbl (
  id               INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  surname          VARCHAR(20)  NOT NULL,
  first_name             VARCHAR(15)  NOT NULL,
  family_name      VARCHAR(20)  NOT NULL,
  birthday         DATE         NOT NULL,
  email            VARCHAR(20),
  icq              VARCHAR(20),
  skype            VARCHAR(20),
  id_leader INT,
  id_department   INT NOT NULL ,
  CONSTRAINT LEADER_FK FOREIGN KEY (id_leader) REFERENCES employee_tbl (id)
);

CREATE TABLE department_tbl (
  id                 INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_department    VARCHAR(40)  NOT NULL,
  id_head_department INT,
  CONSTRAINT HEAD_DEPARTMENT_FK FOREIGN KEY (id_head_department) REFERENCES employee_tbl (id)
);

ALTER TABLE employee_tbl ADD CONSTRAINT FOREIGN KEY (id_department) REFERENCES department_tbl (id);

CREATE TABLE telephone_tbl (
  id               INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  telephone_number VARCHAR(12)  NOT NULL UNIQUE,
  type_telephone   VARCHAR(20)  NOT NULL
);

CREATE TABLE telephone_employee_tbl (
  id_employee  INT NOT NULL,
  id_telephone INT NOT NULL,
  CONSTRAINT EMPLOYEE_TEL_FK FOREIGN KEY (id_employee) REFERENCES employee_tbl (id),
  CONSTRAINT TELEPHONE_FK FOREIGN KEY (id_telephone) REFERENCES telephone_tbl (id)
);

CREATE TABLE address_tbl (
  id      INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  address VARCHAR(50)  NOT NULL
);

CREATE TABLE address_employee_tbl (
  id_employee INT NOT NULL,
  id_address  INT NOT NULL,
  CONSTRAINT EMPLOYEE_ADR_FK FOREIGN KEY (id_employee) REFERENCES employee_tbl (id),
  CONSTRAINT ADDRESS_FK FOREIGN KEY (id_address) REFERENCES address_tbl (id)
);
