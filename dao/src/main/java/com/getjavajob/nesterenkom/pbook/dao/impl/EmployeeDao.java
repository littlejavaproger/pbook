package com.getjavajob.nesterenkom.pbook.dao.impl;

import com.getjavajob.nesterenkom.pbook.dao.GenericDao;
import com.getjavajob.nesterenkom.pbook.dao.exception.PBookDaoException;
import com.getjavajob.nesterenkom.pbook.model.Employee;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDao extends GenericDao<Employee> {
    private static final String INSERT =
            "INSERT INTO " +
                    "employee_tbl (surname, first_name, family_name, birthday, email, icq, skype, id_leader, id_department) " +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";

    private static final String UPDATE_BY_ID =
            "UPDATE employee_tbl " +
                    "SET surname = ?, first_name = ?, family_name = ?, birthday = ?, email = ?, icq = ?, skype = ?, id_leader = ?, id_department = ? " +
                    "WHERE id = ?;";

    private static final String SELECT_BY_ID =
            "SELECT  id, surname, first_name, family_name, birthday, email, icq, skype, id_leader, id_department " +
                    "FROM employee_tbl " +
                    "WHERE id = ?;";

    private static final String SELECT_BY_ID_WITHOUT_DEPARTMENT =
            "SELECT  id, surname, first_name, family_name, birthday, email, icq, skype, id_leader " +
                    "FROM employee_tbl " +
                    "WHERE id = ?;";

    private static final String DELETE_BY_ID = "DELETE FROM employee_tbl WHERE id = ?;";

    private static final String SELECT_ALL = "SELECT id, surname, first_name, family_name, birthday, email, icq, skype, id_leader, id_department " +
            "FROM employee_tbl;";

    private DepartmentDao departmentDao = new DepartmentDao();
    private TelephoneDao telephoneDao = new TelephoneDao();
    private AddressDao addressDao = new AddressDao();

    private DataSource dataSource;

    public EmployeeDao() {
    }

    public EmployeeDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    
    @Override
    public void add(Employee employee) {
        try (Connection connection = this.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
                setInsertQueryParameters(preparedStatement, employee);
                preparedStatement.executeUpdate();
                int generatedId = getGeneratedID(preparedStatement);
                employee.setId(generatedId);
                addEmployeeTelephones(employee);
                addEmployeeAddress(employee);
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw e;
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while add employee " + employee, e);
        }
    }


    @Override
    public void update(Employee employee) {
        try (Connection connection = this.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BY_ID)) {
                setUpdateQueryParameters(preparedStatement, employee);
                preparedStatement.executeUpdate();
                updateEmployeeTelephones(employee);
                updateEmployeeAddress(employee);
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw e;
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while update employee " + employee, e);
        }
    }

    @Override
    public void delete(int id) {
        try (Connection connection = this.dataSource.getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID)) {
                telephoneDao.deleteEmployeeTelephones(id);
                addressDao.deleteEmployeeAddress(id);
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw e;
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while delete by id employee; id = " + id, e);
        }
    }

    @Override
    public Employee get(int id) {
        Employee employee = null;
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    employee = createEmployeeFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while get by id employee; id = " + id, e);
        }

        return employee;
    }

    @Override
    public List<Employee> getAll() {
        List<Employee> allEmployees = new ArrayList<>();
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL)) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                allEmployees.add(
                        createEmployeeFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while get all employees", e);
        }

        return allEmployees;
    }

    Employee getWithoutDepartment(int id) {
        Employee employee = null;
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID_WITHOUT_DEPARTMENT)) {
            preparedStatement.setInt(1, id);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    employee = createEmployeeWithoutDepartment(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while getWithoutDepartment by id employee; id = " + id, e);
        }

        return employee;
    }


    private Employee createEmployeeFromResultSet(ResultSet resultSet) throws SQLException {
        Employee employee = createEmployeeWithoutDepartment(resultSet);
        employee.setDepartment(departmentDao.get(resultSet.getInt("id_department")));
        return employee;
    }

    private Employee createEmployeeWithoutDepartment(ResultSet resultSet) throws SQLException {
        Employee employee = new Employee();

        employee.setId(resultSet.getInt("id"));
        employee.setSurname(resultSet.getString("surname"));
        employee.setName(resultSet.getString("first_name"));
        employee.setMiddleName(resultSet.getString("family_name"));
        employee.setBirthday(resultSet.getDate("birthday"));
        employee.setEmail(resultSet.getString("email"));
        employee.setIcq(resultSet.getString("icq"));
        employee.setSkype(resultSet.getString("skype"));
        int leaderId = resultSet.getInt("id_leader");
        if (leaderId != 0) {
            employee.setLeader(get(leaderId));
        }
        employee.setTelephones(telephoneDao.getEmployeeTelephones(employee.getId()));
        employee.setAddresses(addressDao.getEmployeeAddress(employee.getId()));
        return employee;
    }

    private void addEmployeeTelephones(Employee employee) throws SQLException {
        telephoneDao.addEmployeeTelephones(employee.getId(), employee.getTelephones());
    }

    private void addEmployeeAddress(Employee employee) {
        addressDao.addEmployeeAddresses(employee.getId(), employee.getAddresses());
    }

    private void updateEmployeeTelephones(Employee employee) throws SQLException {
        telephoneDao.deleteEmployeeTelephones(employee.getId());
        telephoneDao.updateEmployeeTelephones(employee.getId(), employee.getTelephones());
    }

    private void updateEmployeeAddress(Employee employee) throws SQLException {
        addressDao.deleteEmployeeAddress(employee.getId());
        addressDao.updateEmployeeAddress(employee.getId(), employee.getAddresses());
    }

    private void setInsertQueryParameters(PreparedStatement preparedStatement, Employee employee) throws SQLException {
        preparedStatement.setString(1, employee.getSurname());
        preparedStatement.setString(2, employee.getName());
        preparedStatement.setString(3, employee.getMiddleName());
        try {
            preparedStatement.setDate(4, new Date(employee.getBirthday().getTime()));
        }catch (NullPointerException e){
            System.out.println("WTF - Where my birthday? " + e);
        }
        if (employee.getEmail() != null) {
            preparedStatement.setString(5, employee.getEmail());
        } else {
            preparedStatement.setNull(5, Types.VARCHAR);
        }
        if (employee.getIcq() != null) {
            preparedStatement.setString(6, employee.getIcq());
        } else {
            preparedStatement.setNull(6, Types.VARCHAR);
        }
        if (employee.getSkype() != null) {
            preparedStatement.setString(7, employee.getSkype());
        } else {
            preparedStatement.setNull(7, Types.VARCHAR);
        }
        if (employee.getLeader() != null) {
            preparedStatement.setInt(8, employee.getLeader().getId());
        } else {
            preparedStatement.setNull(8, Types.INTEGER);
        }
            preparedStatement.setInt(9, employee.getDepartment().getId());
    }

    private void setUpdateQueryParameters(PreparedStatement preparedStatement, Employee employee) throws SQLException {
        setInsertQueryParameters(preparedStatement, employee);
        preparedStatement.setInt(10, employee.getId());
    }
}