package com.getjavajob.nesterenkom.pbook.repository.impl;

import com.getjavajob.nesterenkom.pbook.model.Telephone;
import com.getjavajob.nesterenkom.pbook.repository.TelephoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class TelephoneRepositoryImpl implements TelephoneRepository<Telephone> {

    private static final Logger logger = LoggerFactory.getLogger(TelephoneRepositoryImpl.class);

    @Autowired
    private EntityManager entityManager;

    @Override
    public Telephone add(Telephone telephone) {
        logger.info("[DAO] Telephone method add start");
        logger.debug("Dao layer method=[add] telephone [ %s ]", telephone);

        if (telephone.getNumber() == null || telephone.getType() == null){
            logger.error("Dao layer method=[add] telephone number %s is null or type %s is null", telephone.getNumber(), telephone.getType());
        }
        logger.info("[DAO] Telephone method add end");

        return entityManager.merge(telephone);
    }

    @Override
    public Telephone update(Telephone telephone) {
        logger.info("[DAO] Telephone method update start");
        logger.debug("Dao layer method=[update] telephone [ %s ]", telephone);

        if (telephone.getNumber() == null || telephone.getType() == null){
            logger.error("Dao layer method=[update] telephone number %s is null or type %s is null", telephone.getNumber(), telephone.getType());
        }
        logger.info("[DAO] Telephone method update end");

        return entityManager.merge(telephone);
    }

    @Override
    public void delete(int id) {
        logger.info("[DAO] Telephone method delete start");
        logger.debug("Dao layer method=[delete] telephone id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[delete] telephone id={%s} not found", id);
        }
        logger.info("[DAO] Telephone method delete end");

        entityManager.remove(findById(id));
    }

    @Override
    public Telephone findById(int id) {
        logger.info("[DAO] Telephone method findById start");
        logger.debug("Dao layer method=[find by id] telephone id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[find by id] telephone id={%s} not found", id);
        }
        logger.info("[DAO] Telephone method findById end");

        return entityManager.find(Telephone.class, id);
    }

    @Override
    public List<Telephone> findAll() {
        logger.info("[DAO] Telephone method findAll start");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Telephone> criteriaQuery = criteriaBuilder.createQuery(Telephone.class);
        CriteriaQuery<Telephone> select = criteriaQuery.select(criteriaQuery.from(Telephone.class));
        List<Telephone> telephones = entityManager.createQuery(select).getResultList();
        logger.debug("Dao layer method=[find all] list telephones %s", telephones);
        logger.info("[DAO] Telephone method findAll end");

        return telephones;
    }
}
