package com.getjavajob.nesterenkom.pbook.dao.pool;

import javax.sql.DataSource;
import java.sql.Connection;

public class DataSourceHolder {
    private static DataSource dataSource;

    public static DataSource getDataSource() {
        return dataSource;
    }

    public static void setDataSource(DataSource dataSource) {
        DataSourceHolder.dataSource = dataSource;
    }
}
