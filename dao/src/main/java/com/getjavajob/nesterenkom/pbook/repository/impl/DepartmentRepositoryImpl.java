package com.getjavajob.nesterenkom.pbook.repository.impl;

import com.getjavajob.nesterenkom.pbook.model.Department;
import com.getjavajob.nesterenkom.pbook.repository.DepartmentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class DepartmentRepositoryImpl implements DepartmentRepository<Department> {

    private static final Logger logger = LoggerFactory.getLogger(DepartmentRepositoryImpl.class);

    private EntityManager entityManager;

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    public void setEntityManager(EntityManager entityManager) {
        this. entityManager = entityManager;
    }

    @Override
    public Department add(Department department) {
        logger.info("[DAO] Department method add start");
        logger.debug("Dao layer method=[add] department [ %s ]", department);

        if (department.getName() == null) {
            logger.error("Dao layer method=[add] department name %s is null", department.getName());
        }else if(department.getHeadDepartment() == null){
            logger.error("Dao layer method=[add] department header %s is null", department.getHeadDepartment());
        }
        logger.info("[DAO] Department method add end");

        return entityManager.merge(department);
    }

    @Override
    public Department update(Department department) {
        logger.info("[DAO] Department method update start");
        logger.debug("Dao layer method=[update] department [ %s ]", department);

        if (department.getName() == null){
            logger.error("Dao layer method=[update] department name %s is null", department.getName());
        }else if(department.getHeadDepartment() == null){
            logger.error("Dao layer method=[update] department header %s is null", department.getHeadDepartment());
        }
        logger.info("[DAO] Department method update end");

        return entityManager.merge(department);
    }

    @Override
    public void delete(int id) {
        logger.info("[DAO] Department method delete start");
        logger.debug("Dao layer method=[delete] department id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[delete] department id={%s} not found", id);
        }
        logger.info("[DAO] Department method delete end");

        entityManager.remove(findById(id));
    }

    @Override
    public Department findById(int id) {
        logger.info("[DAO] Department method findById start");
        logger.debug("Dao layer method=[find by id] department id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[find by id] department id={%s} not found", id);
        }
        logger.info("[DAO] Department method findById end");

        return entityManager.find(Department.class, id);
    }

    @Override
    public List<Department> findAll() {
        logger.info("[DAO] Department method findAll start");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Department> criteriaQuery = criteriaBuilder.createQuery(Department.class);
        CriteriaQuery<Department> select = criteriaQuery.select(criteriaQuery.from(Department.class));
        List<Department> departments = entityManager.createQuery(select).getResultList();
        logger.debug("Dao layer method=[find all] list addresses %s", departments);
        logger.info("[DAO] Department method findAll end");

        return departments;
    }
}
