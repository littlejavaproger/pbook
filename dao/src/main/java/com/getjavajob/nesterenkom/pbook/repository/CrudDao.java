package com.getjavajob.nesterenkom.pbook.repository;

import java.util.List;

public interface CrudDao<T> {
    T add(T entity);

    T update(T entity);

    void delete(int id);

    T findById(int id);

    List<T> findAll();
}
