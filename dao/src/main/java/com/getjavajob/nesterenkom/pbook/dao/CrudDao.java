package com.getjavajob.nesterenkom.pbook.dao;

import java.util.List;

public interface CrudDao<T> {
    public void add(T entity);

    public void update(T entity);

    public void delete(int id);

    public T get(int id);

    public List<T> getAll();
}
