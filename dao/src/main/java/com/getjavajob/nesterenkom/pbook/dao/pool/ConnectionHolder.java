package com.getjavajob.nesterenkom.pbook.dao.pool;

import java.sql.Connection;

public class ConnectionHolder {
    private Connection connection;
    private Integer countOpenConnection;

    ConnectionHolder(Connection connection, Integer countOpenConnection) {
        this.connection = connection;
        this.countOpenConnection = countOpenConnection;
    }

    Connection getConnection() {
        return connection;
    }

    Integer getCountOpenConnection() {
        return countOpenConnection;
    }

    void incrementOpenCountConnection(){
        ++countOpenConnection;
    }

    void decrementOpenCountConnection(){
        --countOpenConnection;
    }
}
