package com.getjavajob.nesterenkom.pbook.dao.exception;

public class PBookDaoException extends RuntimeException{
    public PBookDaoException() {
    }

    public PBookDaoException(String message) {
        super(message);
    }

    public PBookDaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public PBookDaoException(Throwable cause) {
        super(cause);
    }

    public PBookDaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
