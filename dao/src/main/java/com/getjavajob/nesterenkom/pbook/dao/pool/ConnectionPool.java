package com.getjavajob.nesterenkom.pbook.dao.pool;

import com.getjavajob.nesterenkom.pbook.dao.exception.PBookDaoException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class ConnectionPool {
    private static final String PROPERTY = "config.properties";
    /**
     * DB property
     */
    private static final String PROPERTY_URL = "database.url";
    private static final String PROPERTY_USER = "database.user";
    private static final String PROPERTY_PASSWORD = "database.password";
    private static final String PROPERTY_POOL_SIZE = "database.pool_size";
    private static final String PROPERTY_DRIVER = "database.driver";
    /**
     * DP property values after init property file
     */
    private String DB_URL;
    private String DB_USER;
    private String DB_PASSWORD;
    private int DB_POOL_SIZE;

    private ThreadLocal<ConnectionHolder> connectionHolder = new ThreadLocal<>();
    private static ConnectionPool instance = new ConnectionPool();

    private BlockingQueue<Connection> availableConnections;
    private int poolCapacity = 0;

    private ConnectionPool(){
        initProperties();
        availableConnections = new LinkedBlockingQueue<>(DB_POOL_SIZE);
        attachShutDownHook();
    }

    public static ConnectionPool getInstance(){
        return instance;
    }

    public Connection getConnection(){
        if (connectionHolder.get() == null){
            connectionHolder.set(new ConnectionHolder(makeProxy(getConnectionFromPool()), 0));
        }
        connectionHolder.get().incrementOpenCountConnection();
        return connectionHolder.get().getConnection();
    }

    void releaseConnection(Connection connection){
        connectionHolder.get().decrementOpenCountConnection();
        if(connectionHolder.get().getCountOpenConnection() == 0){
            try {
                availableConnections.put(connection);
            }catch (InterruptedException e){
                throw new PBookDaoException("Exception while releasing connection", e);
            }
            connectionHolder.remove();
        }
    }

    /**
     * @return connection from pool
     */
    private Connection getConnectionFromPool(){
        checkAndCreateConnection();
        try {
            return availableConnections.take();
        } catch (InterruptedException e) {
            throw new PBookDaoException("Exception while getting connection from pool", e);
        }
    }

    /**
     * check for necessity create new connection and create if need
     */
    private synchronized void checkAndCreateConnection(){
        if (availableConnections.size() == 0){
            if(poolCapacity < DB_POOL_SIZE){
                createConnection();
            }
        }
    }

    private void createConnection(){
        try {
            Connection newConnection = DriverManager.getConnection(
                    DB_URL,
                    DB_USER,
                    DB_PASSWORD);
            settingConnection(newConnection);
            availableConnections.put(newConnection);
            ++poolCapacity;
        } catch (SQLException | InterruptedException e) {
            throw new PBookDaoException("Exception while creating new connection for pool", e);
        }
    }

    private void settingConnection(Connection connection){
        try {
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while setting connection", e);
        }
    }

    public synchronized void shutdown(){
        try {
            for(Connection connection : availableConnections){
                connection.close();
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while shutdown ConnectionPool", e);
        }

    }

    private void attachShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                shutdown();
            }
        });
    }


    private void initProperties() {
        Properties connectionProperty = new Properties();

        try {
            connectionProperty.load(ConnectionPool.class.getClassLoader().getResourceAsStream(PROPERTY));
            Class.forName(connectionProperty.getProperty(PROPERTY_DRIVER));
            DB_URL = connectionProperty.getProperty(PROPERTY_URL);
            DB_USER = connectionProperty.getProperty(PROPERTY_USER);
            DB_PASSWORD = connectionProperty.getProperty(PROPERTY_PASSWORD);
            DB_POOL_SIZE = Integer.parseInt(connectionProperty.getProperty(PROPERTY_POOL_SIZE));
        } catch (IOException | ClassNotFoundException e) {
            throw new PBookDaoException("Exception init property " + e);
        }
    }

    private Connection makeProxy(Connection connection){
        return (Connection) Proxy.newProxyInstance(Connection.class.getClassLoader(), new Class[]{Connection.class}, new ConnectionProxyInvocationHandler(connection));
    }

    class ConnectionProxyInvocationHandler implements InvocationHandler{
        private Connection connection;

        public ConnectionProxyInvocationHandler(Connection connection) {
            this.connection = connection;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if(method.getName().equals("close")) {
                ConnectionPool.getInstance().releaseConnection(connection);
                return null;
            }else if (method.getName().equals("equals")) {
                Object arg = args[0];

                if(Proxy.isProxyClass(arg.getClass()) && Proxy.getInvocationHandler(arg) instanceof ConnectionProxyInvocationHandler){
                    return method.invoke(connection, ((ConnectionProxyInvocationHandler)Proxy.getInvocationHandler(args[0])).connection);
                }
            }
            return method.invoke(connection, args);
        }
    }
}
