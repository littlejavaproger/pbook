package com.getjavajob.nesterenkom.pbook.repository.impl;

import com.getjavajob.nesterenkom.pbook.model.Address;
import com.getjavajob.nesterenkom.pbook.repository.AddressRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class AddressRepositoryImpl implements AddressRepository<Address> {

    private static final Logger logger = LoggerFactory.getLogger(AddressRepositoryImpl.class);

    @Autowired
    private EntityManager entityManager;

    @Override
    public Address add(Address address) {
        logger.info("[DAO] Address method add start");
        logger.debug("Dao layer method=[add] address [ %s ]", address);

        if (address.getAddress() == null || address.getType() == null) {
            logger.error("Dao layer method=[add] address %s is null or type %s is null", address.getAddress(), address.getType());
        }
        logger.info("[DAO] Address method add end");

        return entityManager.merge(address);
    }

    @Override
    public Address update(Address address) {
        logger.info("[DAO] Address method update start");
        logger.debug("Dao layer method=[update] address [ %s ]", address);

        if (address.getAddress() == null || address.getType() == null){
            logger.error("Dao layer method=[update] address %s is null or type %s is null", address.getAddress(), address.getType());
        }
        logger.info("[DAO] Address method update end");

        return entityManager.merge(address);
    }

    @Override
    public void delete(int id) {
        logger.info("[DAO] Address method delete start");
        logger.debug("Dao layer method=[delete] address id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[delete] address id={%s} not found", id);
        }
        logger.info("[DAO] Address method delete end");

        entityManager.remove(findById(id));
    }

    @Override
    public Address findById(int id) {
        logger.info("[DAO] Address method findById start");
        logger.debug("Dao layer method=[find by id] address id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[find by id] address id={%s} not found", id);
        }
        logger.info("[DAO] Address method findById end");

        return entityManager.find(Address.class, id);
    }

    @Override
    public List<Address> findAll() {
        logger.info("[DAO] Address method findAll start");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Address> criteriaQuery = criteriaBuilder.createQuery(Address.class);
        CriteriaQuery<Address> select = criteriaQuery.select(criteriaQuery.from(Address.class));
        List<Address> addresses = entityManager.createQuery(select).getResultList();
        logger.debug("Dao layer method=[find all] list addresses %s", addresses);
        logger.info("[DAO] Address method findAll end");

        return addresses;
    }
}
