package com.getjavajob.nesterenkom.pbook.dao;

import com.getjavajob.nesterenkom.pbook.dao.pool.ConnectionPool;
import com.getjavajob.nesterenkom.pbook.model.BaseEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class GenericDao<T> extends BaseEntity implements CrudDao<T> {

    protected Connection getConnection(){
        return ConnectionPool.getInstance().getConnection();
    }

    protected int getGeneratedID(PreparedStatement preparedStatement) throws SQLException {
        ResultSet keys = preparedStatement.getGeneratedKeys();
        if(keys.next()){
            return keys.getInt(1);
        }
        return EMPTY_ID;
    }
//
//    @Override
//    public T get(int id) {
//        Connection connection = null;
//        try {
//            connection = ConnectionPool.getConnection();
//            try (PreparedStatement prepareStatement = connection.prepareStatement(getSelectByIdStatement())) {
//                prepareStatement.setInt(1, id);
//                try (ResultSet resultSet = prepareStatement.executeQuery()) {
//                    return createInstanceFromResult(resultSet);
//                }
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            //  ConnectionPool.releaseConnection(connection);
//        }
//    }
}
