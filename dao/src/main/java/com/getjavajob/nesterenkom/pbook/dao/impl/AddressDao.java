package com.getjavajob.nesterenkom.pbook.dao.impl;

import com.getjavajob.nesterenkom.pbook.dao.GenericDao;
import com.getjavajob.nesterenkom.pbook.model.Address;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AddressDao extends GenericDao<Address>{
   private static final String INSERT_ADDRESS = "INSERT INTO address_tbl (address) VALUES (?);";
   private static final String INSERT_ADDRESS_EMPLOYEE = "INSERT INTO  address_employee_tbl (id_employee, id_address) " +
           "VALUES (?, ?)";
   private static final String SELECT_ADDRESS_EMPLOYEE = "SELECT address " +
           "FROM address_tbl " +
           "INNER JOIN address_employee_tbl " +
           "ON address_tbl.id = address_employee_tbl.id_address " +
           "WHERE id_employee = ?;";
    private static final String SELECT_ADDRESS_ID = "SELECT * FROM address_tbl WHERE address = ?";
    private static final String DELETE_ADDRESS_EMPLOYEE = "DELETE FROM address_employee_tbl " +
            "WHERE id_employee = ?;";
   private static final String DELETE_ADDRESS = "DELETE FROM address_tbl " +
           "WHERE NOT EXISTS " +
           "(" +
           "SELECT id_address FROM address_employee_tbl WHERE address_tbl.id = address_employee_tbl.id_address" +
           ");";

    void addEmployeeAddresses(int employeeId, List<Address> addresses) {
        try {
            if(addresses != null && addresses.size() > 0){
                for(Address address : addresses){
                    int addressId;
                    try {
                        //Try add address
                        addressId = addToAddressTBL(address);
                    }catch (SQLException e){
                        //if address already exist get it's id
                        addressId = getIdFromAddressTBL(address);
                    }
                    if(addressId != EMPTY_ID){
                        addToAddressEmployee(employeeId, addressId);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    private int getIdFromAddressTBL(Address address) throws SQLException {
        int id = EMPTY_ID;
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ADDRESS_ID))
        {
            preparedStatement.setString(1, address.getAddress());
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if(resultSet.next()){
                    id = resultSet.getInt("id");
                }
            }
        }

        return id;
    }

    void deleteEmployeeAddress(int empId) throws SQLException {
        deleteFromAddressEmployeeTBL(empId);
        deleteFromAddressTBL();
    }

    void updateEmployeeAddress(int employeeId, List<Address> addresses) throws SQLException {
        addEmployeeAddresses(employeeId, addresses);
    }

    List<Address> getEmployeeAddress(int employeeId) throws SQLException {
        List<Address> addresses;
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ADDRESS_EMPLOYEE))
        {
            preparedStatement.setInt(1, employeeId);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                addresses = new ArrayList<>();
                while (resultSet.next()){
                    addresses.add(createPhoneFromResultSet(resultSet));
                }
            }
        }
        return addresses;
    }

    private void addToAddressEmployee(int employeeId, int addressId) throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ADDRESS_EMPLOYEE))
        {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.setInt(2, addressId);
            preparedStatement.executeUpdate();
        }
    }

    private int addToAddressTBL(Address address) throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_ADDRESS, Statement.RETURN_GENERATED_KEYS))
        {
            preparedStatement.setString(1, address.getAddress());
            preparedStatement.executeUpdate();

            return getGeneratedID(preparedStatement);
        }
    }

    private void deleteFromAddressEmployeeTBL(int employeeId) throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ADDRESS_EMPLOYEE))
        {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.executeUpdate();
        }
    }

    private void deleteFromAddressTBL() throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ADDRESS))
        {
            preparedStatement.executeUpdate();
        }
    }

    private Address createPhoneFromResultSet(ResultSet resultSet) throws SQLException {
        Address address = new Address();

        address.setAddress(resultSet.getString(1));

        return address;
    }

    @Override
    public void add(Address entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Address entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Address get(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Address> getAll() {
        throw new UnsupportedOperationException();
    }
}
