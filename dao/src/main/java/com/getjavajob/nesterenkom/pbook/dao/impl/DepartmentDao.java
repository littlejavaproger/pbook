package com.getjavajob.nesterenkom.pbook.dao.impl;

import com.getjavajob.nesterenkom.pbook.dao.GenericDao;
import com.getjavajob.nesterenkom.pbook.dao.exception.PBookDaoException;
import com.getjavajob.nesterenkom.pbook.model.Department;
import com.getjavajob.nesterenkom.pbook.model.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DepartmentDao extends GenericDao<Department>{

    private static final String INSERT = "INSERT INTO department_tbl (name_department, id_head_department) VALUES (?, ?);";
    private static final String UPDATE_BY_ID = "UPDATE department_tbl SET name_department = ?, id_head_department = ? WHERE id = ?;";
    private static final String DELETE_BY_ID = "DELETE FROM department_tbl WHERE id = ?;";
    private static final String SELECT_BY_ID = "SELECT id, name_department, id_head_department FROM department_tbl WHERE id = ?;";
    private static final String SELECT_ALL = "SELECT id, name_department, id_head_department FROM department_tbl;";

    @Override
    public void add(Department department) {
        try(Connection connection = getConnection())
        {
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS))
            {
                setInsertQueryParameters(preparedStatement, department);
                preparedStatement.executeUpdate();
                int generatedID = getGeneratedID(preparedStatement);
                department.setId(generatedID);
                connection.commit();
            }catch (SQLException e) {
                connection.rollback();
                throw e;
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while add department " + department, e);
        }
    }

    @Override
    public void update(Department department) {
        try(Connection connection = getConnection())
        {
            try(PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_BY_ID))
            {
                setUpdateQueryParameters(preparedStatement, department);
                preparedStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                throw e;
            }
        }catch (SQLException e) {
            throw new PBookDaoException("Exception while update department " + department, e);
        }
    }

    @Override
    public void delete(int id) {
        try(Connection connection = getConnection())
        {
            try(PreparedStatement preparedStatement = connection.prepareStatement(DELETE_BY_ID))
            {
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
                connection.commit();
            }catch (SQLException e){
                connection.rollback();
                throw e;
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while delete by id department; id = " + id, e);
        }
    }

    @Override
    public Department get(int id) {
        Department department = null;
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_ID))
        {
            preparedStatement.setInt(1, id);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if(resultSet.next()){
                    department = createDepartmentFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while get by id department; id = " + id, e);
        }
        return department;
    }

    @Override
    public List<Department> getAll() {
        List<Department> allDepartments = new ArrayList<>();
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL))
        {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                allDepartments.add(
                        createDepartmentFromResultSet(resultSet));
            }
        } catch (SQLException e) {
            throw new PBookDaoException("Exception while get all departments", e);
        }
        return allDepartments;
    }

    private Department createDepartmentFromResultSet(ResultSet resultSet) throws SQLException{
        Department department = new Department();
        EmployeeDao employeeDao = new EmployeeDao();

        department.setId(resultSet.getInt("id"));
        department.setName(resultSet.getString("name_department"));
        Employee headDepartment = employeeDao.getWithoutDepartment(resultSet.getInt("id_head_department"));
        if (headDepartment != null){
            headDepartment.setDepartment(department);
        }
        department.setHeadDepartment(headDepartment);

        return department;
    }

    private void setInsertQueryParameters(PreparedStatement preparedStatement, Department department) throws SQLException{
        preparedStatement.setString(1, department.getName());
        if (department.getHeadDepartment() == null){
            preparedStatement.setNull(2, Types.INTEGER);
        }else {
            preparedStatement.setInt(2, department.getHeadDepartment().getId());
        }
    }

    private void setUpdateQueryParameters(PreparedStatement preparedStatement, Department department) throws SQLException{
        setInsertQueryParameters(preparedStatement, department);
        preparedStatement.setInt(3, department.getId());
    }
}
