package com.getjavajob.nesterenkom.pbook.repository.impl;

import com.getjavajob.nesterenkom.pbook.model.Employee;
import com.getjavajob.nesterenkom.pbook.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository<Employee> {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeRepositoryImpl.class);

    private EntityManager entityManager;

    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    public void setEntityManager(EntityManager entityManager) {
        this. entityManager = entityManager;
    }

    @Override
    public Employee add(Employee employee) {
        logger.info("[DAO] Employee method add start");
        logger.debug("Dao layer method=[add] employee [ %s ]", employee);

        if (employee.getSurname() == null || employee.getName() == null || employee.getMiddleName() == null) {
            logger.error("Dao layer method=[add] employee surname %s is null or name %s is null or middle name %s is null",
                    employee.getSurname(), employee.getName(), employee.getMiddleName());
        }else if(employee.getLeader().getSurname() == null || employee.getLeader().getName() == null || employee.getLeader().getMiddleName() == null){
            logger.error("Dao layer method=[add] employee leader surname %s is null or name %s is null or middle name %s is null",
                    employee.getLeader().getSurname(), employee.getLeader().getName(), employee.getLeader().getMiddleName());
        }
        logger.info("[DAO] Employee method add end");

        return entityManager.merge(employee);
    }

    @Override
    public Employee update(Employee employee) {
        logger.info("[DAO] Employee method update start");
        logger.debug("Dao layer method=[update] employee [ %s ]", employee);

        if (employee.getSurname() == null || employee.getName() == null || employee.getMiddleName() == null){
            logger.error("Dao layer method=[update] employee surname %s is null or name %s is null or middle name %s is null",
                    employee.getSurname(), employee.getName(), employee.getMiddleName());
        }else if(employee.getLeader().getSurname() == null || employee.getLeader().getName() == null || employee.getLeader().getMiddleName() == null){
            logger.error("Dao layer method=[update] employee leader surname %s is null or name %s is null or middle name %s is null",
                    employee.getLeader().getSurname(), employee.getLeader().getName(), employee.getLeader().getMiddleName());
        }
        logger.info("[DAO] Employee method update end");

        return entityManager.merge(employee);
    }

    @Override
    public void delete(int id) {
        logger.info("[DAO] Employee method delete start");
        logger.debug("Dao layer method=[delete] employee id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[delete] employee id={%s} not found", id);
        }
        logger.info("[DAO] Employee method delete end");

        entityManager.remove(findById(id));
    }

    @Override
    public Employee findById(int id) {
        logger.info("[DAO] Employee method findById start");
        logger.debug("Dao layer method=[find by id] employee id={%s}", id);

        if (id < 0){
            logger.error("Dao layer method=[find by id] employee id={%s} not found", id);
        }
        logger.info("[DAO] Employee method findById end");

        return entityManager.find(Employee.class, id);
    }

    @Override
    public List<Employee> findAll() {
        logger.info("[DAO] Employee method findAll start");
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        CriteriaQuery<Employee> select = criteriaQuery.select(criteriaQuery.from(Employee.class));
        List<Employee> employees = entityManager.createQuery(select).getResultList();
        logger.debug("Dao layer method=[find all] list addresses %s", employees);
        logger.info("[DAO] Employee method findAll end");

        return employees;
    }
}
