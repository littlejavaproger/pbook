package com.getjavajob.nesterenkom.pbook.dao.impl;

import com.getjavajob.nesterenkom.pbook.dao.GenericDao;
import com.getjavajob.nesterenkom.pbook.model.Telephone;
import com.getjavajob.nesterenkom.pbook.model.TypeTelephone;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TelephoneDao extends GenericDao<Telephone>{
    private static final String INSERT_TELEPHONE = "INSERT INTO telephone_tbl (telephone_number, type_telephone) VALUES (?, ?);";
    private static final String INSERT_TELEPHONE_EMPLOYEE =
            "INSERT INTO telephone_employee_tbl (id_employee, id_telephone) " +
                    "VALUES (?, ?);";
    private static final String SELECT_TELEPHONE_EMPLOYEE =
            "SELECT telephone_number, type_telephone " +
                    "FROM telephone_tbl INNER JOIN telephone_employee_tbl ON " +
                    "telephone_tbl.id = telephone_employee_tbl.id_telephone " +
                    "WHERE id_employee = ?;";
    private static final String SELECT_TELEPHONE_ID =
            "SELECT id FROM telephone_tbl WHERE telephone_number = ?";

    private static final String DELETE_TELEPHONE_EMPLOYEE =  "DELETE FROM telephone_employee_tbl WHERE id_employee = ?;";
    private static final String DELETE_TELEPHONE =
            "DELETE FROM telephone_tbl " +
                    "WHERE NOT EXISTS " +
                    "(" +
                    "SELECT id_telephone FROM telephone_employee_tbl WHERE telephone_tbl.id = telephone_employee_tbl.id_telephone" +
                    ");";



    void addEmployeeTelephones(int employeeId, List<Telephone> telephones) {
        try {
            if(telephones != null && telephones.size() > 0){
                for(Telephone telephone : telephones){
                    int phoneId;
                    try {
                        //Try add telephone
                        phoneId = addToTelephoneTBL(telephone);
                    }catch (SQLException e){
                        //if telephone already exist get it's id
                        phoneId = getIdFromTelephoneTBL(telephone);
                    }
                    if(phoneId != EMPTY_ID){
                        addToTelephoneEmployee(employeeId, phoneId);
                    }
                }
            }
        } catch (SQLException e) {
            System.err.println(e);
        }
    }

    private int getIdFromTelephoneTBL(Telephone telephone) throws SQLException {
        int id = EMPTY_ID;
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TELEPHONE_ID))
        {
            preparedStatement.setString(1, telephone.getNumber());
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                if(resultSet.next()){
                    id = resultSet.getInt("id");
                }
            }
        }

        return id;
    }

    void deleteEmployeeTelephones(int empId) throws SQLException {
        deleteFromTelephoneEmployeeTBL(empId);
        deleteFromTelephoneTBL();
    }

    void updateEmployeeTelephones(int empId, List<Telephone> phones) throws SQLException {
        addEmployeeTelephones(empId, phones);
    }

    List<Telephone> getEmployeeTelephones(int employeeId) throws SQLException {
        List<Telephone> phones;
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SELECT_TELEPHONE_EMPLOYEE))
        {
            preparedStatement.setInt(1, employeeId);
            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                phones = new ArrayList<>();
                while (resultSet.next()){
                    phones.add(createPhoneFromResultSet(resultSet));
                }
            }
        }
        return phones;
    }

    private void addToTelephoneEmployee(int employeeId, int telephoneId) throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TELEPHONE_EMPLOYEE))
        {
            preparedStatement.setInt(1, employeeId);
            preparedStatement.setInt(2, telephoneId);
            preparedStatement.executeUpdate();
        }
    }

    private int addToTelephoneTBL(Telephone phone) throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TELEPHONE, Statement.RETURN_GENERATED_KEYS))
        {
            preparedStatement.setString(1, phone.getNumber());
            preparedStatement.setString(2, String.valueOf(phone.getType()));
            preparedStatement.executeUpdate();

            return getGeneratedID(preparedStatement);
        }
    }

    private void deleteFromTelephoneEmployeeTBL(int empId) throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TELEPHONE_EMPLOYEE))
        {
            preparedStatement.setInt(1, empId);
            preparedStatement.executeUpdate();
        }
    }

    private void deleteFromTelephoneTBL() throws SQLException {
        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TELEPHONE))
        {
            preparedStatement.executeUpdate();
        }
    }

    private Telephone createPhoneFromResultSet(ResultSet resultSet) throws SQLException {
        Telephone telephone = new Telephone();

        telephone.setNumber(resultSet.getString(1));
        telephone.setType(TypeTelephone.valueOf(resultSet.getString(2)));

        return telephone;
    }

    @Override
    public void add(Telephone telephone) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void update(Telephone telephone) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Telephone get(int id) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List getAll() {
        throw new UnsupportedOperationException();
    }
}
