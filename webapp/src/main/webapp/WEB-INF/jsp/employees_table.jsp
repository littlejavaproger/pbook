<%--
  Created by IntelliJ IDEA.
  User: Nestor
  Date: 29.11.2015
  Time: 23:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/fonts/favicon.ico" type="image/x-icon">

    <title>Cписок сотрудников</title>

    <link href="<c:url value="/resources/css/third_party/bootstrap/dataTables.bootstrap.min.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/page/employee_table_info.css" />" rel="stylesheet">
    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>
</head>
<body>
<%@ include file="templete_page.jsp" %>
<%--<div id="sEmployeeFromResponse"></div>--%>
<div class="animated zoomIn">
    <h1>Список сотрудников</h1>
    <table id="pbook-table" class="table table-striped" style="width: 100%;">
        <thead>
        <tr>
            <td>#</td>
            <td>ФИО</td>
            <td>Рабочий телефон</td>
            <td>Департамент</td>
            <td>Действия</td>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td>#</td>
            <td>ФИО</td>
            <td>Рабочий телефон</td>
            <td>Департамент</td>
            <td>Действия</td>
        </tr>
        </tfoot>
        <tbody>
        <c:forEach var="employee" items="${employees}">
            <c:set var="count" value="${count + 1}" scope="page"/>
            <tr>
                <td>
                    <c:out value="${count}"/>
                </td>
                <td>${employee.surname} ${employee.name} ${employee.middleName}</td>
                <td>
                    <c:forEach var="telephone" items="${employee.telephones}">
                        <c:if test="${telephone.type == 'WORK'}">
                            ${telephone.number}
                        </c:if>
                    </c:forEach >
                </td>
                <td>${employee.department.name}</td>
                <td>
                    <%--INFO EMPLOYEE--%>
                    <div class="button_actions">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="bottom"
                            title="Показать информацию о сотруднике"><a
                            href='<c:url value="/PBook/employee/info?id=${employee.id}" />'><span
                            class="glyphicon glyphicon-search"></span></a></button>
                    <%--UPDATE EMPLOYEE--%>
                    <button type="button" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="bottom"
                            title="Изменить данные о сотруднике"><a
                            href='<c:url value="/PBook/employee/update?id=${employee.id}" />'><span
                            class="glyphicon glyphicon-file"></span></a></button>
                    <%--REMOVE EMPLOYEE--%>
                    <button id="action-btn" type="button" class="btn btn-default btn-lg" data-toggle="tooltip" data-placement="bottom"
                            title="Удалить сотрудника"><a
                            href='<c:url value="/PBook/employee/delete?id=${employee.id}" />'><span
                            class="glyphicon glyphicon-trash"></span></a></button>
                    </div>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog" style="z-index: 1401">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Вы уверены, что хотите удалить сотрудника - ${employee.surname} ${employee.name} ${employee.middleName}?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-success" id="modal-save-employee-btn">Да</button>
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#pbook-table').DataTable();
    } );
</script>
</body>
</html>
