<%--
  Created by IntelliJ IDEA.
  User: Nestor
  Date: 28.02.2016
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список департаментов</title>
</head>
<body>
<%@ include file="templete_page.jsp" %>

<div class="animated zoomIn">
    <h1>Список департаментов</h1>
    <table id="pbook-table" class="table table-striped" style="width: 100%;">
        <thead>
        <tr>
            <td>#</td>
            <td>Название департамента</td>
            <td>Руководитель департамента</td>
            <td>Действия</td>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td>#</td>
            <td>Название департамента</td>
            <td>Руководитель департамента</td>
            <td>Действия</td>
        </tr>
        </tfoot>
        <tbody>
        <c:forEach var="department" items="${departments}">
            <c:set var="count" value="${count + 1}" scope="page"/>
            <tr>
                <td>
                    <c:out value="${count}"/>
                </td>
                <td>${department.name}</td>
                <td>${department.headDepartment.surname} ${department.headDepartment.name} ${department.headDepartment.middleName}</td>
                <td>
                        <%--INFO DEPARTMENT--%>
                    <div class="button_actions">
                        <button type="button" class="btn btn-default btn-lg" data-toggle="tooltip"
                                data-placement="bottom"
                                title="Показать информацию о департаменте"><a
                                href='<c:url value="/PBook/department/info?id=${department.id}" />'><span
                                class="glyphicon glyphicon-search"></span></a></button>
                            <%--UPDATE DEPARTMENT--%>
                        <button type="button" class="btn btn-default btn-lg" data-toggle="tooltip"
                                data-placement="bottom"
                                title="Изменить данные о департаменте"><a
                                href='<c:url value="/PBook/department/update?id=${department.id}" />'><span
                                class="glyphicon glyphicon-file"></span></a></button>
                            <%--REMOVE DEPARTMENT--%>
                        <button id="action-btn" type="button" class="btn btn-default btn-lg" data-toggle="tooltip"
                                data-placement="bottom"
                                title="Удалить департамент"><a
                                href='<c:url value="/PBook/department/delete?id=${department.id}" />'><span
                                class="glyphicon glyphicon-trash"></span></a></button>
                    </div>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog" style="z-index: 1401">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Вы уверены, что хотите удалить департамент - ${department.name}?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-success" id="modal-save-department-btn">Да</button>
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#pbook-table').DataTable();
    });
</script>
</body>
</html>
