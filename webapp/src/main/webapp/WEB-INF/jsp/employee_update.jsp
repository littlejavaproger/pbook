<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 01.12.15
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <%--Иконка окна браузера--%>
    <link rel="shortcut icon" href="../../resources/fonts/favicon.ico" type="image/x-icon">

    <title>Update Employee</title>
    <%@ include file="templete_page.jsp" %>

    <%--CSS employee update--%>
    <link rel="stylesheet" href="<c:url value="/resources/css/page/employee_update.css" />">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <%--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>
    <%--<![endif]-->--%>
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>
                    <div class="row">
                        <div class="col-md-2">
                            <img id="blah" name="photoEmployee"
                                 src="<c:choose><c:when test="${photo ne null}"><c:out value="${photo}" /></c:when><c:otherwise>../../resources/img/user_photo_default.png</c:otherwise></c:choose>"
                                 alt="your image" width="160" height="160"/>
                            <a id="ok" type="button" href="#"><span class="glyphicon glyphicon-ok-circle"></span></a>
                            <a type="button" onclick="removePhoto()" href="#"><span
                                    class="glyphicon glyphicon-remove-circle"></span></a>
                        </div>
                        <div class="col-md-8" style="font-size: 25px;">
                            ${employee.surname}
                            <br>${employee.name}
                            <br>${employee.middleName}
                        </div>
                    </div>
                </legend>
                <form name="update" id="employee-form"
                      action="${pageContext.request.contextPath}/PBook/employee/doUpdate" method="post">
                    <input type="hidden" name="id" value="${employee.id}"/>
                    <table class="table table-striped">
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Фото</td>
                            <td>
                                <input id="input1" type='file' onchange="handleFiles(this.files)"
                                       style="display: none"/>
                                <input id="idPhoto" name="photoBase64" type="hidden" value="${photo}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Фамилия</td>
                            <td>
                                <input type="text" name="surname" value="${employee.surname}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Имя</td>
                            <td>
                                <input type="text" name="name" value="${employee.name}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Отчество</td>
                            <td>
                                <input type="text" name="middleName" value="${employee.middleName}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> День рождения</td>
                            <td>
                                <div class="container">
                                    <div class="row">
                                        <div class='col-sm-6'>
                                            <div class="form-group">
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' name="birthday" class="form-control"
                                                           value="${birthday}"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <%--НАЧАЛО ТЕЛЕФОНА--%>
                        <tr>
                            <td><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> Телефоны</td>
                            <td>
                                <div class="container">
                                    <div class="row clearfix">
                                        <div class="col-md-12 table-responsive">
                                            <table class="table table-bordered table-hover table-sortable"
                                                   id="tab_logic_phone">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">
                                                        Телефон
                                                    </th>
                                                    <th class="text-center">
                                                        Тип
                                                    </th>
                                                    <th class="text-center">
                                                        Операция
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id='phn0' data-id="0" class="hidden">
                                                    <td data-name="telephone">
                                                        <input type="text" name='telephone0'
                                                               placeholder='+Х-(ХХХ)-ХХХ-ХХ-ХХ'
                                                               class="form-control"
                                                               style="width: 350px;"/>
                                                    </td>
                                                    <td data-name="type_phone">
                                                        <select name="newType">
                                                            <c:forEach items="${allTelephoneType}"
                                                                       var="currentTelephoneType">
                                                                <c:choose>
                                                                    <c:when test="${currentTelephoneType == telephone.type}">
                                                                        <option value="${currentTelephoneType}"
                                                                                selected>
                                                                                ${currentTelephoneType}
                                                                        </option>
                                                                    </c:when>

                                                                    <c:otherwise>
                                                                        <option value="${currentTelephoneType}">
                                                                                ${currentTelephoneType}
                                                                        </option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                    <td data-name="del">
                                                        <div class="phone_button" style="text-align: center">
                                                            <button name=del0"
                                                                    class='btn btn-danger glyphicon glyphicon-trash row-remove'
                                                                    style="width: 50px;"></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <c:forEach items="${employee.telephones}" var="telephone" varStatus="i">
                                                    <tr>
                                                        <td>
                                                            <input type="text" name="telephoneNumber[i]"
                                                                   value="${telephone.number}">
                                                        </td>
                                                        <td>
                                                            <select name="typeTelephone">
                                                                <c:forEach items="${allTelephoneType}"
                                                                           var="currentTelephoneType">
                                                                    <c:choose>
                                                                        <c:when test="${currentTelephoneType == telephone.type}">
                                                                            <option value="${currentTelephoneType}"
                                                                                    selected>
                                                                                    ${currentTelephoneType}
                                                                            </option>
                                                                        </c:when>

                                                                        <c:otherwise>
                                                                            <option value="${currentTelephoneType}">
                                                                                    ${currentTelephoneType}
                                                                            </option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:forEach>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <div class="phone_button" style="text-align: center">
                                                                <button name="deleteTelephone" style="width: 50px"
                                                                        type="submit"
                                                                        class="btn btn-primary btn-sm"
                                                                        data-toggle="tooltip" data-placement="bottom"
                                                                        title="Удалить телефон">
                                                                    <a href='<c:url value="/PBook/employee/doUpdate?telephoneNumber=${telephone.number}&telephoneType=${telephone.type}&employeeId=${employee.id}" />'>
                                                                        <span class="glyphicon glyphicon-trash"></span>
                                                                    </a>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <a id="add_row_phone" class="btn btn-default pull-right">Добавить строку для
                                        телефона</a>
                                </div>
                            </td>
                        </tr>
                        <%--КОНЕЦ ТЕЛЕФОНА--%>
                        <tr>
                            <td><i class="fa fa-building"></i> Адреса</td>
                            <td>
                                <div class="container">
                                    <div class="row clearfix">
                                        <div class="col-md-12 table-responsive">
                                            <table class="table table-bordered table-hover table-sortable"
                                                   id="tab_logic_address">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">
                                                        Адрес
                                                    </th>
                                                    <th class="text-center">
                                                        Тип
                                                    </th>
                                                    <th class="text-center">
                                                        Операция
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id='addr0' data-id="0" class="hidden">
                                                    <td data-name="address">
                                                        <input type="text" name='address0'
                                                               placeholder='Адрес'
                                                               class="form-control"
                                                               style="width: 350px;"/>
                                                    </td>
                                                    <td data-name="type_address">
                                                        <select name="newType">
                                                            <c:forEach items="${allAddressType}"
                                                                       var="currentAddressType">
                                                                <c:choose>
                                                                    <c:when test="${currentAddressType == address.type}">
                                                                        <option value="${currentAddressType}" selected>
                                                                                ${currentAddressType}
                                                                        </option>
                                                                    </c:when>

                                                                    <c:otherwise>
                                                                        <option value="${currentAddressType}">
                                                                                ${currentAddressType}
                                                                        </option>
                                                                    </c:otherwise>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </select>
                                                    </td>
                                                    <td data-name="del">
                                                        <div class="phone_button" style="text-align: center">
                                                            <button name=del0"
                                                                    class='btn btn-danger glyphicon glyphicon-trash row-remove'
                                                                    style="width: 50px;"></button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <c:forEach items="${employee.addresses}" var="address">
                                                    <tr>
                                                        <td>
                                                            <input type="text" name="addressName"
                                                                   value="${address.address}">
                                                        </td>
                                                        <td>
                                                            <select name="newType">
                                                                <c:forEach items="${allAddressType}"
                                                                           var="currentAddressType">
                                                                    <c:choose>
                                                                        <c:when test="${currentAddressType == address.type}">
                                                                            <option value="${currentAddressType}"
                                                                                    selected>
                                                                                    ${currentAddressType}
                                                                            </option>
                                                                        </c:when>

                                                                        <c:otherwise>
                                                                            <option value="${currentAddressType}">
                                                                                    ${currentAddressType}
                                                                            </option>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:forEach>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <div class="phone_button" style="text-align: center">
                                                                <button name="deleteTelephone" style="width: 50px"
                                                                        type="button"
                                                                        class="btn btn-primary btn-sm"
                                                                        data-toggle="tooltip" data-placement="bottom"
                                                                        title="Удалить адрес"><a
                                                                        href='<c:url value="/PBook/employee/doUpdate?telephoneNumber=${address.address}&telephoneType=${address.type}&employeeId=${employee.id}" />'><span
                                                                        class="glyphicon glyphicon-trash"></span></a>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <a id="add_row_address" class="btn btn-default pull-right">Добавить строку для
                                        адреса</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-at"></i> Email</td>
                            <td><input type="text" name="email" value="${employee.email}"/></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-weixin"></i> icq</td>
                            <td><input type="text" name="icq" value="${employee.icq}"/></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-skype"></i> Skype</td>
                            <td><input type="text" name="skype" value="${employee.skype}"/></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-user"></i> Начальник</td>
                            <td>
                                <select name="leaderId">
                                    <c:forEach items="${allEmployees}" var="currentEmployee">
                                        <c:choose>
                                            <c:when test="${currentEmployee.id == employee.leader.id}">
                                                <option value="${currentEmployee.id}" selected>
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:when>

                                            <c:otherwise>
                                                <option value="${currentEmployee.id}">
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-users"></i> Департамент</td>
                            <td>
                                <select name="departmentId">
                                    <c:forEach items="${allDepartments}" var="currentDepartment">
                                        <c:choose>
                                            <c:when test="${currentDepartment.id == employee.department.id}">
                                                <option value="${currentDepartment.id}" selected>
                                                        ${currentDepartment.name}
                                                </option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${currentDepartment.id}">
                                                        ${currentDepartment.name}
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="btn-action">
                                    <button id="action-btn" type="submit" name="Save" class="btn btn-default btn-lg">
                                        <span class="glyphicon glyphicon-save"></span> Сохранить
                                    </button>

                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </fieldset>
        </div>
    </div>
</div>
<c:import url="footer.jsp"></c:import>
 <div class="modal fade" id="modal-confirm">
        <div class="modal-dialog" style="z-index: 1401">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h2>Сохранить изменения?</h2>
                </div>
                <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                    <button type="button" class="btn btn-raised btn-success" id="modal-save-employee-btn">Да</button>
                    <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
                </div>
            </div>
        </div>
    </div>
<jsp:include page="js_script.jsp"/>
</body>
</html>
