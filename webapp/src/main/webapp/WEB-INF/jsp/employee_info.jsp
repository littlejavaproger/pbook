<%--
  Created by IntelliJ IDEA.
  User: Nestor
  Date: 05.12.2015
  Time: 15:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="../../resources/fonts/favicon.ico" type="image/x-icon">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PBook employee information</title>

    <%--CSS--%>
    <!-- Bootstrap -->
    <link href="<c:url value="../../resources/css/third_party/bootstrap/bootstrap.min.css" />" rel="stylesheet">
    <%--Employee info--%>
    <link href="<c:url value="../../resources/css/page/employee_info.css" />" rel="stylesheet">
    <%--JS--%>
    <%--Jquery--%>
    <link href="<c:url value="../../resources/js/jquery/jquery.min.js" />" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<c:url value="../../resources/js/bootstrap/bootstrap.min.js" />" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <%--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>
    <%--<![endif]-->--%>
</head>
<body>

<c:import url="templete_page.jsp"></c:import>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>
                    <div class="row">
                        <div class="col-md-2" style="width: 10%;">
                            <img src="<c:choose><c:when test="${photo ne null}"><c:out value="${photo}" /></c:when><c:otherwise>../../resources/img/user_photo_default.png</c:otherwise></c:choose>"
                                    style="width: 160px; height: 160px;"
                                    class="img-thumbnail" alt="Thumbnail Image">
                        </div>
                        <div class="col-md-8" style="font-size: 25px;">
                            ${employee.surname}
                            <br>${employee.name}
                            <br>${employee.middleName}
                        </div>
                    </div>
                </legend>
                <table class="table table-striped">
                    <tr>
                        <td>Фамилия</td>
                        <td>${employee.surname}</td>
                    </tr>
                    <tr>
                        <td>Имя</td>
                        <td>${employee.name}</td>
                    </tr>
                    <tr>
                        <td>Отчество</td>
                        <td>${employee.middleName}</td>
                    </tr>
                    <tr>
                        <td>День рождения</td>
                        <td>${employee.birthday}</td>
                    </tr>
                    <c:forEach items="${employee.telephones}" var="telephone">
                        <tr>
                            <td>
                                <c:choose>
                                    <c:when test="${telephone.type == 'WORK'}">
                                        Рабочий телефон
                                    </c:when>
                                    <c:when test="${telephone.type == 'MOBILE'}">
                                        Мобильный телефон
                                    </c:when>
                                    <c:when test="${telephone.type == 'DEPARTMENT'}">
                                        Телефон департамента
                                    </c:when>
                                    <c:otherwise>
                                        Домашний телефон
                                    </c:otherwise>
                                </c:choose>
                            </td>
                            <td>
                                    ${telephone.number}
                            </td>
                        </tr>
                    </c:forEach>
                    <tr>
                        <td>Рабочий Адресс</td>
                        <td>
                            <c:forEach items="${employee.addresses}" var="address" begin="1" end="1">
                                ${address.address}
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <td>Домашний Адресс</td>
                        <td>
                            <c:forEach items="${employee.addresses}" var="address" begin="0" end="0">
                                ${address.address}
                            </c:forEach>
                        </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>${employee.email}</td>
                    </tr>
                    <tr>
                        <td>icq</td>
                        <td>${employee.icq}</td>
                    </tr>
                    <tr>
                        <td>Skype</td>
                        <td>${employee.skype}</td>
                    </tr>
                    <tr>
                        <td>Начальник</td>
                        <td>
                            <c:if test="${employee.leader == null}">
                                Отсутствует начальник, или сам является начальником
                            </c:if>
                            ${employee.leader.surname} ${employee.leader.name} ${employee.leader.middleName}
                        </td>
                    </tr>
                    <tr>
                        <td>Департамент</td>
                        <td>${employee.department.name}</td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
</div>
<div class="btn_back">
    <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-arrow-left"></span><a
            href='<c:url value="/PBook/employee/all_info" />'> Back</a></button>
</div>
</body>
</html>
