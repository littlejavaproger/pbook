<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 01.12.15
  Time: 15:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <%--Иконка окна браузера--%>
    <link rel="shortcut icon" href="../../resources/fonts/favicon.ico" type="image/x-icon">

    <title>Update Department</title>
    <%@ include file="templete_page.jsp" %>

    <%--CSS employee update--%>
    <link rel="stylesheet" href="<c:url value="/resources/css/page/employee_update.css" />">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <%--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>
    <%--<![endif]-->--%>
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>
                    <div class="row">
                        <div class="col-md-8" style="font-size: 25px;">
                            ${department.name}
                        </div>
                    </div>
                </legend>
                <form name="update" id="department-form" action="${pageContext.request.contextPath}/PBook/department/doUpdate" method="post">
                    <input type="hidden" name="id" value="${department.id}"/>
                    <table class="table table-striped">
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Название департамента</td>
                            <td>
                                <input type="text" name="name" value="${department.name}"/>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-user"></i> Руководитель департамента</td>
                            <td>
                                <select name="headDepartmentId">
                                    <c:forEach items="${allEmployees}" var="currentEmployee">
                                        <c:choose>
                                            <c:when test="${currentEmployee.id == department.headDepartment.id}">
                                                <option value="${currentEmployee.id}" selected>
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:when>

                                            <c:otherwise>
                                                <option value="${currentEmployee.id}">
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="btn-action">
                                    <button id="action-btn" type="submit" name="Save" class="btn btn-default btn-lg">
                                        <span class="glyphicon glyphicon-save"></span> Сохранить
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </fieldset>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog" style="z-index: 1401">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Сохранить изменения?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-success" id="modal-save-department-btn">Да</button>
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<c:import url="footer.jsp"></c:import>
</body>
</html>
