<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 02.12.15
  Time: 0:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <%--Иконка окна браузера--%>
    <link rel="shortcut icon" href="../../resources/fonts/favicon.ico" type="image/x-icon">

    <title>Create Employee</title>

    <%@ include file="templete_page.jsp" %>
    <%--CSS--%>
    <%--Employee info--%>
    <link rel="stylesheet" href="<c:url value="/resources/css/page/employee_info.css" />">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <%--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>
    <%--<![endif]-->--%>
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <form id="employee-form" action="${pageContext.request.contextPath}/PBook/employee/doCreate" method="post">
                    <input type="hidden" name="id" value="${employee.id}"/>
                    <table class="table table-striped">
                        <tr>
                            <td><span class="glyphicon glyphicon-camera" aria-hidden="true"></span> Фото</td>
                            <td>
                                <img id="blah" name="photoEmployee"
                                     src="<c:choose><c:when test="${photo ne null}"><c:out value="${photo}" /></c:when><c:otherwise>../../resources/img/user_photo_default.png</c:otherwise></c:choose>"
                                     alt="your image" width="160" height="160"/>
                                <a id="ok" type="button" href="#"><span
                                        class="glyphicon glyphicon-ok-circle"></span></a>
                                <a type="button" onclick="removePhoto()" href="#"><span
                                        class="glyphicon glyphicon-remove-circle"></span></a>
                                <input id="input1" type='file' onchange="handleFiles(this.files)"
                                       style="display: none"/>
                                <input id="idPhoto" name="photoBase64" type="hidden"/>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Фамилия</td>
                            <td><input type="text" name="surname" placeholder="Фамилия"/>
                            </td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Имя</td>
                            <td><input type="text" name="name" placeholder="Имя"/></td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Отчество</td>
                            <td><input type="text" name="middle_name" placeholder="Отчество"/></td>
                        </tr>
                        <tr>
                            <td><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> День рождения</td>
                            <td>
                                <div class="container">
                                    <div class="row">
                                        <div class='col-sm-6'>
                                            <div class="form-group">
                                                <div class='input-group date' id='datetimepicker1'>
                                                    <input type='text' name="birthday" class="form-control"
                                                           placeholder="XX/XX/XX"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> Телефон
                            </td>
                            <td>
                                <div class="telephon_tbl">
                                    <table style="margin-left: 0; margin-right: 0;" class="table table-bordered">
                                        <tr>
                                            <td><label>Номер Телефона</label></td>
                                            <td><label>Тип</label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="newTelephoneNumber"
                                                       placeholder="+Х-(ХХХ)-ХХХ-ХХ-ХХ">
                                            </td>
                                            <td>
                                                <select name="newTypeTelephone">
                                                    <c:forEach items="${allTelephoneType}" var="currentTelephoneType">
                                                        <c:choose>
                                                            <c:when test="${currentTelephoneType == telephone.type}">
                                                                <option value="${currentTelephoneType}" selected>
                                                                        ${currentTelephoneType}
                                                                </option>
                                                            </c:when>

                                                            <c:otherwise>
                                                                <option value="${currentTelephoneType}">
                                                                        ${currentTelephoneType}
                                                                </option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-building"></i> Адреса</td>
                            <td>
                                <div class="telephon_tbl">
                                    <table style="margin-left: 0; margin-right: 0;" class="table table-bordered">
                                        <tr>
                                            <td><label>Адрес</label></td>
                                            <td><label>Тип</label></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="text" name="newAddressName" placeholder="Адресс">
                                            </td>
                                            <td>
                                                <select name="newTypeAddress">
                                                    <c:forEach items="${allAddressType}" var="currentAddressType">
                                                        <c:choose>
                                                            <c:when test="${currentAddressType == address.type}">
                                                                <option value="${currentAddressType}" selected>
                                                                        ${currentAddressType}
                                                                </option>
                                                            </c:when>

                                                            <c:otherwise>
                                                                <option value="${currentAddressType}">
                                                                        ${currentAddressType}
                                                                </option>
                                                            </c:otherwise>
                                                        </c:choose>
                                                    </c:forEach>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-at"></i> Email</td>
                            <td><input type="text" name="email" placeholder="Email"/></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-weixin"></i> icq</td>
                            <td><input type="text" name="icq" placeholder="ICQ"/></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-skype"></i> Skype</td>
                            <td><input type="text" name="skype" placeholder="Skype"/></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-user"></i> Начальник</td>
                            <td>
                                <select name="leaderId">
                                    <c:forEach items="${allEmployees}" var="currentEmployee">
                                        <c:choose>
                                            <c:when test="${currentEmployee.id == employee.leader.id}">
                                                <option value="${currentEmployee.id}" selected>
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:when>

                                            <c:otherwise>
                                                <option value="${currentEmployee.id}">
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-users"></i> Департамент</td>
                            <td>
                                <select name="departmentId">
                                    <c:forEach items="${allDepartments}" var="currentDepartment">
                                        <c:choose>
                                            <c:when test="${currentDepartment.id == employee.department.id}">
                                                <option value="${currentDepartment.id}" selected>
                                                        ${currentDepartment.name}
                                                </option>
                                            </c:when>
                                            <c:otherwise>
                                                <option value="${currentDepartment.id}">
                                                        ${currentDepartment.name}
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div class="btn_back">
                        <button id="action-btn" type="submit" name="Save" class="btn btn-default btn-lg"><span
                                class="glyphicon glyphicon-save"></span>
                            Сохранить
                        </button>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../resources/js/preview_photo_user.js"></script>
<c:import url="footer.jsp"></c:import>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog" style="z-index: 1401">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Сохранить сотрудника?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-success" id="modal-save-employee-btn">Да</button>
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<jsp:include page="js_script.jsp"/>
</body>
</html>
