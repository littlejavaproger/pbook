<%--
  Created by IntelliJ IDEA.
  User: apple
  Date: 02.12.15
  Time: 0:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <%--Иконка окна браузера--%>
    <link rel="shortcut icon" href="../../resources/fonts/favicon.ico" type="image/x-icon">

    <title>Create Department</title>

    <%@ include file="templete_page.jsp" %>
    <%--CSS--%>
    <%--Employee info--%>
    <link rel="stylesheet" href="<c:url value="/resources/css/page/employee_info.css" />">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <%--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>
    <%--<![endif]-->--%>
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <form id="department-form" action="${pageContext.request.contextPath}/PBook//department/doCreate" method="post">
                    <input type="hidden" name="id" value="${department.id}"/>
                    <table class="table table-striped">
                        <tr>
                            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Название департамента</td>
                            <td><input type="text" name="name" placeholder="Название департамента"/></td>
                        </tr>
                        <tr>
                            <td><i class="fa fa-user"></i> Руководитель департамента</td>
                            <td>
                                <select name="headDepartmentId">
                                    <c:forEach items="${allEmployees}" var="currentEmployee">
                                        <c:choose>
                                            <c:when test="${currentEmployee.id == department.headDepartment.id}">
                                                <option value="${currentEmployee.id}" selected>
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:when>

                                            <c:otherwise>
                                                <option value="${currentEmployee.id}">
                                                        ${currentEmployee.surname} ${currentEmployee.name} ${currentEmployee.middleName}
                                                </option>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <div class="btn_back">
                        <button id="action-byn" type="submit" name="Save" class="btn btn-default btn-lg"><span
                                class="glyphicon glyphicon-save"></span>
                            Сохранить
                        </button>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog" style="z-index: 1401">
        <div class="modal-content">
            <div class="modal-body text-center">
                <h2>Сохранить департамент?</h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-success" id="modal-save-department-btn">Да</button>
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal">Нет</button>
            </div>
        </div>
    </div>
</div>
<c:import url="footer.jsp"></c:import>
</body>
</html>
