<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--TODO Почистить от мусора--%>
<%--
  Created by IntelliJ IDEA.
  User: Nestor
  Date: 29.11.2015
  Time: 17:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--<link rel="shortcut icon" href="../../resources/fonts/favicon.ico" type="image/x-icon">--%>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/fonts/favicon.ico" type="image/x-icon">

    <meta charset="UTF-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login Page</title>
    <meta name="msapplication-TileColor" content="#5bc0de">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../resources/css/third_party/bootstrap/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../resources/css/third_party/fonts_css/font-awesome.min.css">
    <%--Animate--%>
    <link rel="stylesheet" href="../../resources/css/third_party/animate/animate.css">
    <%--PBook stylesheet--%>
    <link rel="stylesheet" href="../../resources/css/page/login_page.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <!--<script src="assets/lib/html5shiv/html5shiv.js"></script>-->
    <%--<script src="assets/lib/respond/respond.min.js"></script>--%>
    <%--<![endif]-->--%>

    <!--Modernizr-->
    <script src="../../resources/js/modernizr/modernizr.min.js"></script>
</head>
<body
        class="login"
<%--class="animated fadeInDown"--%>
        >
<div class="login_page">
    <div class="form-signin">
        <div class="text-center">
            <img src="../../resources/img/logo.png" alt="PBook Logo" class="animated fadeInDown">
        </div>
        <hr>
        <div class="tab-content">
            <div id="login" class="tab-pane active">
                <%--<form action="${pageContext.request.contextPath}/PBook/login" method="post">--%>
                <form class="form-signin" action="<c:url value="${pageContext.request.contextPath}sign-in_process"/>" method="post">

                    <input type="hidden" value="${param.get("from")}" name="from">
                    <%--<p class="bg-danger">--%>

                    <c:if test="${error != null}">
                        <div class="alert alert-danger" role="alert">
                                ${error }
                        </div>
                    </c:if>
                    <%--</p>--%>
                    <p class="text-muted text-center">
                        Введите имя пользователя и пароль
                    </p>
                    <input name="username" type="text" placeholder="Имя пользователя" class="form-control top">
                    <input name="password" type="password" placeholder="Пароль" class="form-control bottom">

                    <div class="checkbox">
                        <label>
                            <input name="remember" value="remember" type="checkbox"> Запомнить пароль
                        </label>
                    </div>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Авторизоваться</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!--jQuery -->
<script src="../../resources/js/jquery/jquery.min.js"></script>

<!--Bootstrap -->
<script src="../../resources/js/bootstrap/bootstrap.min.js"></script>

<footer class="Footer bg-dark dker">
    <p>2015 © PBook-0.0.1 (Organization telephone book)</p>
</footer>
</body>
</html>
