<%--
  Created by IntelliJ IDEA.
  User: Nestor
  Date: 05.12.2015
  Time: 15:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" href="../../resources/fonts/favicon.ico" type="image/x-icon">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Info Department</title>

    <%--CSS--%>
    <!-- Bootstrap -->
    <link href="<c:url value="../../resources/css/third_party/bootstrap/bootstrap.min.css" />" rel="stylesheet">
    <%--Employee info--%>
    <link href="<c:url value="../../resources/css/page/employee_info.css" />" rel="stylesheet">
    <%--JS--%>
    <%--Jquery--%>
    <link href="<c:url value="../../resources/js/jquery/jquery.min.js" />" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<c:url value="../../resources/js/bootstrap/bootstrap.min.js" />" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <%--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>
    <%--<![endif]-->--%>
</head>
<body>

<c:import url="templete_page.jsp"></c:import>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span12">
            <fieldset>
                <legend>
                    <div class="row">
                        <div class="col-md-8" style="font-size: 25px;">
                            ${department.name}
                        </div>
                    </div>
                </legend>
                <table class="table table-striped">
                    <tr>
                        <td>Название департамента</td>
                        <td>${department.name}</td>
                    </tr>
                    <tr>
                        <td>Руководитель департамента</td>
                        <td>${department.headDepartment.surname} ${department.headDepartment.name} ${department.headDepartment.middleName}</td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>
</div>
<div class="btn_back">
    <button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-arrow-left"></span><a
            href='<c:url value="/PBook/department/all_info" />'> Back</a></button>
</div>
</body>
</html>
