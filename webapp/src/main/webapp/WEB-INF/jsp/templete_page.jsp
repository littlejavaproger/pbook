<%--
  Created by IntelliJ IDEA.
  User: Nestor
  Date: 05.12.2015
  Time: 13:00
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<!DOCTYPE html>--%>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>PBook tables employees</title>

    <%--CSS--%>
    <%--Jquery--%>
    <link rel="stylesheet" href="<c:url value="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>">
    <!-- Bootstrap -->
    <link href="<c:url value="/resources/css/third_party/bootstrap/bootstrap.min.css" />" rel="stylesheet">
    <!-- animate-->
    <link href="<c:url value="/resources/css/third_party/animate/animate.css" />" rel="stylesheet">
    <!-- templete css-->
    <link href="<c:url value="/resources/css/page/templete.css" />" rel="stylesheet">

    <link rel="stylesheet"
          href="<c:url value="../../resources/css/third_party/bootstrap/bootstrap-datetimepicker.min.css" />">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <%--JavaScript--%>

    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--%>

    <%--<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->--%>
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <%--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--%>
    <%--<![endif]-->--%>
</head>
<body>
<!-- second menu bar -->
<nav class="navbar navbar-default navbar-static">
    <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#b-menu-2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img src="../../resources/img/logo_mini.png" alt="logo"></a>
    </div>
    <!-- submenu elements for #b-menu-2 -->
    <div class="collapse navbar-collapse" id="b-menu-2">
        <ul class="nav navbar-nav">
            <li class="active"><a href='<c:url value="/PBook/employee/all_info" />'><span
                    class="glyphicon glyphicon-home"></span> Home</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                        class="glyphicon glyphicon-list"></span> Other <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li class="divider"></li>
                    <li class="nav-header"><span class="select"><h5>Сотрудник</h5></span></li>
                    <li class="divider"></li>
                    <li><a href='<c:url value="/PBook//employee/create" />'>Создать сотрудника</a></li>
                    <li><a href='<c:url value="/PBook/employee/all_info" />'>Показать всех сотрудников</a></li>
                    <li class="divider"></li>
                    <li class="nav-header"><span class="select"><h5>Департамент</h5></span></li>
                    <li class="divider"></li>
                    <li><a href='<c:url value="/PBook//department/create" />'>Создать департамент</a></li>
                    <li><a href='<c:url value="/PBook/department/all_info" />'>Показать все департаменты</a></li>
                </ul>
            </li>
            <li>
                <div class="ui-widget" style="padding-top: 7px;">
                    <input id="employeeFIO" type="text" class="form-control" placeholder="Search">
                </div>
            </li>
        </ul>

        <%--USER INFO--%>
        <ul class="nav navbar-nav navbar-right">
            <li class="user_name" style="top: 15px; right: 15px;">
                    <span class="glyphicon glyphicon-user"> ${sessionScope.User}</span>
            </li>
            <li data-toggle="modal" data-target="#my-modal-box">
                <a href="${pageContext.request.contextPath}/PBook/logout?action=Logout">
                    <span class="glyphicon glyphicon-share"></span> Logout
                </a>
            </li>
        </ul>
    </div>
</nav>
<script type="text/javascript">var myContextPath = "<c:url value="/PBook/getEmployees" />"</script>
<script type="text/javascript" src="../../resources/js/jquery/jquery-1.10.2.js"></script>
<script type="text/javascript" src="../../resources/js/jquery/jquery-ui.js"></script>
<script type="text/javascript" src="../../resources/js/jquery/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="../../resources/js/jquery/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../resources/js/bootstrap/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="../../resources/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="../../resources/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="../../resources/js/bootstrap/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="../../resources/js/datepicker_settings.js"></script>
<script type="text/javascript" src="../../resources/js/autocomplete_full_name.js"></script>
</body>
</html>
