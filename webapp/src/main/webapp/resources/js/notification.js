/**
 * Created by Nestor on 08.03.2016.
 */

$("#action-btn").click(function (e) {
    e.preventDefault();
    var validationOk = true;
    if (validationOk == true) {
        $('#modal-confirm').modal('show');
    }
    else {
        $('.top-right').notify({
            message: {text: errorText.val},
            fadeOut: {enabled: true, delay: 3000},
            closable: false,
            type: 'danger'
        }).show();
    }
});

$("#modal-save-employee-btn").click(function (e) {
    $("#employee-form").submit();
    e.preventDefault();
});

$("#modal-save-department-btn").click(function (e) {
    $("#department-form").submit();
    e.preventDefault();
});


