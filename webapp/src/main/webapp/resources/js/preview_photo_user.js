/**
 * Created by Nestor on 06.02.2016.
 */
var okFile = document.getElementById("ok");
     input1 = document.getElementById("input1");

okFile.addEventListener("click", function(e){
    if (input1){
        input1.click();
    }
    e.preventDefault();
}, false);
function handleFiles(files){
    var viewPhoto = document.getElementById("blah");
    var photo = document.getElementById("idPhoto");
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            viewPhoto.setAttribute("src", img.src);
            photo.setAttribute("value", img.src);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(files[0]);
};
function removePhoto(){
    document.getElementById("blah").setAttribute("src", "../../resources/img/user_photo_default.png")
    document.getElementById("idPhoto").setAttribute("value", '');
}