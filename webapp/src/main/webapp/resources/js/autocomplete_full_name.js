/**
 * Created by Nestor on 07.02.2016.
 */
$(document).ready(function () {
        $("#employeeFIO").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: myContextPath,
                    data: {
                        filter: request.term
                    },
                    method: "GET",
                    success: function (data) {
                        response($.map(data, function (employee, i) {
                            return {
                                value: employee.surname + ' ' + employee.name + ' ' + employee.middleName,
                                label: employee.surname + ' ' + employee.name + ' ' + employee.middleName
                            }
                        }));
                    },
                    error: function (data) {
                        console.log("error" + data);
                    }
                });
            },
            minLength: 1
        });
    }
);