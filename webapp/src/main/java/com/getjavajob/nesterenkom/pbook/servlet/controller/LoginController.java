package com.getjavajob.nesterenkom.pbook.servlet.controller;

import com.getjavajob.nesterenkom.pbook.model.User;
import com.getjavajob.nesterenkom.pbook.util.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login(){

        return "login_page";
    }

    @RequestMapping(value = "sign-in_process", method = RequestMethod.POST)
    public void doLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.info("SignInProcess method doPost start");
        String login = req.getParameter("username");
        String password = req.getParameter("password");
        String rememberMe = req.getParameter("remember");
        HttpSession session = req.getSession();

        User user = Users.getUser(login, password);
        logger.debug("User login is [" + (user != null ? user.getLogin() : null) + "] and user password [" + (user != null ? user.getPassword() : null) + "]");
        if (user != null) {
            logger.debug("user not equal null");
            session.setAttribute("User", user);
            if (rememberMe != null && rememberMe.equals("remember")) {
                Cookie cookieLogin = new Cookie("LoginCookie", user.getLogin());
                cookieLogin.setMaxAge(3600);
                resp.addCookie(cookieLogin);
            }
            resp.sendRedirect(req.getContextPath() + "/PBook/employee/all_info");
        } else {
            logger.debug("user equals null");
            req.setAttribute("error", "Неверно введено имя или пароль");
            req.getRequestDispatcher("/WEB-INF/jsp/login_page.jsp").forward(req, resp);
        }
        logger.info("SignInProcess method doPost end");
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public void doLogout(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.info("SignOutProcess method doGet start");
        String action = req.getParameter("action");
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(60);
        if (action == null) {
            logger.debug("SignOutProcess_doGet_action null");
            User user = checkCookie(req);
            if (user == null) {
                req.getRequestDispatcher("/WEB-INF/jsp/login_page.jsp").forward(req, resp);
            } else {
                session.setAttribute("username", user.getLogin());
                resp.sendRedirect(req.getContextPath() + "/PBook/employee/all_info");

            }
        } else {
            if (action.equalsIgnoreCase("logout")) {
                logger.debug("SignOutProcess_logout");

                logger.debug("session ATTRIBUTE User = " + session.getAttribute("User"));
                session.removeAttribute("User");
                logger.debug("session ATTRIBUTE User after update = " + session.getAttribute("User"));

                Cookie[] cookies = req.getCookies();
                if (cookies != null) {
                    for (Cookie ck : cookies) {
                        logger.debug(ck.getName());
                        if (ck.getValue().equals("User") || ck.getValue().equals("Admin")) {
                            logger.debug("Use " + ck.getName());
                            logger.debug(ck.getName() + "- Cookie after value " + ck.getValue());
                            ck.setValue("");
                            logger.debug(ck.getName() + "- Cookie before value " + ck.getValue());
                            logger.debug(ck.getName() + "- Cookie after max age " + ck.getMaxAge());
                            ck.setMaxAge(0);
                            logger.debug(ck.getName() + "- Cookie before max age " + ck.getMaxAge());
                            ck.setPath("/");
                            resp.addCookie(ck);
                        }
                    }
                }
                req.getRequestDispatcher("/WEB-INF/jsp/login_page.jsp").forward(req, resp);
            }
        }
        logger.info("SignOutProcess method doGet end");
    }

    private User checkCookie(HttpServletRequest request) {
        logger.info("SignOutProcess method checkCookie start");
        Cookie[] cookies = request.getCookies();
        User user = null;
        if (cookies == null) {
            return null;
        } else {
            String username = "";
            for (Cookie ck : cookies) {
                if (ck.getName().equalsIgnoreCase("UserCookie")) {
                    username = ck.getValue();
                }
            }
            if (!username.isEmpty()) {
                user = Users.getUser(username);
            }
            logger.info("SignOutProcess method checkCookie end");
            return user;
        }
    }
}
