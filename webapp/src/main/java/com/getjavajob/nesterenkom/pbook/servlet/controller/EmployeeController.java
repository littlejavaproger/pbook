package com.getjavajob.nesterenkom.pbook.servlet.controller;

import com.getjavajob.nesterenkom.pbook.model.*;
import com.getjavajob.nesterenkom.pbook.service.AddressService;
import com.getjavajob.nesterenkom.pbook.service.DepartmentService;
import com.getjavajob.nesterenkom.pbook.service.TelephoneService;
import com.getjavajob.nesterenkom.pbook.service.EmployeeService;
import com.getjavajob.nesterenkom.pbook.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.getjavajob.nesterenkom.pbook.util.Util.convertBlobToString;
import static com.getjavajob.nesterenkom.pbook.util.Util.convertDateToString;

@Controller
@RequestMapping("employee")
public class EmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);
    private static final Logger rolLogger = LoggerFactory.getLogger("RollingFile");

    @Autowired
    private EmployeeService<Employee> employeeService;
    @Autowired
    private TelephoneService<Telephone> telephoneService;
    @Autowired
    private AddressService<Address> addressService;
    @Autowired
    private DepartmentService<Department> departmentService;

    @RequestMapping(value = "/all_info", method = RequestMethod.GET)
    public String employeesInfo(Model model) {
        logger.info("[WEB] EmployeeController method employeesInfo start");
        List<Employee> employees = employeeService.getAll();
        logger.debug("List employees =[%s]", employees);
        model.addAttribute("employees", employees);

        return "employees_table";
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String employeeInfo(@RequestParam(value = "id") int id, Model model) {
        logger.info("[WEB] EmployeeController method employeesInfo start");
        Employee employee = employeeService.get(id);
        logger.debug("Get information employee id={%s}", id);
        model.addAttribute("employee", employee);
        model.addAttribute("photo", Util.convertBlobToString(employee.getPhoto()));
        logger.info("Employee birthday = %s", employee.getBirthday());
        logger.info("[WEB] EmployeeController method employeesInfo end");

        return "employee_info";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView employeeUpdate(@RequestParam(value = "id") int id) {
        logger.info("[WEB] EmployeeController method employeeUpdate start");
        Employee employee = employeeService.get(id);
        logger.debug("GET controller before update employee = {%s} ", employee);
        ModelAndView model = new ModelAndView("employee_update");
        model.addObject("photo", convertBlobToString(employee.getPhoto()));
        model.addObject("birthday", convertDateToString(employee.getBirthday()));
        model.addObject("employee", employee);
        model.addObject("allEmployees", employeeService.getAll());
        model.addObject("allTelephoneType", telephoneService.getAllTypeTelephone());
        model.addObject("allAddressType", addressService.getAllTypeAddress());
        model.addObject("allDepartments", departmentService.getAll());
        logger.info("[WEB] EmployeeController method employeeUpdate end");
        return model;
    }

    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST)
    public void employeeDoUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.info("[WEB] EmployeeController method employeeDoUpdate start");
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");

        Employee employee = employeeService.get(Integer.valueOf(req.getParameter("id")));
        String base64Photo = req.getParameter("photoBase64");
        logger.info("PHOTO BASE64 = {%s} " + base64Photo);
        Blob blobPhoto = Util.convertStringToBlob(base64Photo);
        logger.info("PHOTO BLOB = {%s}" + blobPhoto);
        employee.setPhoto(blobPhoto);
        employee.setSurname(req.getParameter("surname"));
        employee.setName(req.getParameter("name"));
        employee.setMiddleName(req.getParameter("middleName"));
        Date birthDate = Util.convertStringToDate(req.getParameter("birthday"));
        employee.setBirthday(birthDate);
        employee.setEmail(req.getParameter("email"));
        employee.setIcq(req.getParameter("icq"));
        employee.setSkype(req.getParameter("skype"));
        String leaderId = req.getParameter("leaderId");
        employee.setLeader(employeeService.get(Integer.parseInt(leaderId)));
        String departmentId = req.getParameter("departmentId");
        employee.setDepartment(departmentService.get(Integer.parseInt(departmentId)));
        logger.debug("SAVE controller after update employee = {%s} ", employee);
        employeeService.update(employee);

        resp.sendRedirect(req.getContextPath() + "/PBook/employee/all_info");
        logger.info("[WEB] EmployeeController method employeeDoUpdate end");
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView employeeCreate(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.info("[WEB] EmployeeController method employeeCreate start");
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");

        ModelAndView model = new ModelAndView("employee_create");
        model.addObject("allEmployees", employeeService.getAll());
        model.addObject("allTelephoneType", telephoneService.getAllTypeTelephone());
        model.addObject("allAddressType", addressService.getAllTypeAddress());
        model.addObject("allDepartments", departmentService.getAll());
        logger.info("[WEB] EmployeeController method employeeCreate end");

        return model;
    }

    @RequestMapping(value = "/doCreate", method = RequestMethod.POST)
    public void employeeDoCreate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("[WEB] EmployeeController method employeeDoCreate start");
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");

        Employee employee = new Employee();
        employee.setSurname(req.getParameter("surname"));
        employee.setName(req.getParameter("name"));
        employee.setMiddleName(req.getParameter("middle_name"));

        Date birthDate = Util.convertStringToDate(req.getParameter("birthday"));
        employee.setBirthday(birthDate);

        List<Telephone> telephones = new ArrayList<>();
        Telephone telephone = new Telephone(req.getParameter("newTelephoneNumber"), Util.convertStringToTypeTelephone(req.getParameter("newTypeTelephone")));
        telephones.add(telephone);
        employee.setTelephones(telephones);

        List<Address> addresses = new ArrayList<>();
        Address address = new Address(req.getParameter("newAddressName"), Util.convertStringToTypeAddress(req.getParameter("newTypeAddress")));
        addresses.add(address);
        employee.setAddresses(addresses);

        employee.setEmail(req.getParameter("email"));
        employee.setIcq(req.getParameter("icq"));
        employee.setSkype(req.getParameter("skype"));
        String leaderId = req.getParameter("leaderId");
        employee.setLeader(employeeService.get(Integer.parseInt(leaderId)));

        Employee leader = employee.getLeader();
        logger.info("LEADER [%s] employee [%s]",leader, employee);

        String departmentId = req.getParameter("departmentId");
        employee.setDepartment(departmentService.get(Integer.parseInt(departmentId)));

        if (req.getParameter("Save") != null) {
            logger.debug("Saved employee " + employee);

            employeeService.add(employee);
            resp.sendRedirect(req.getContextPath() + "/PBook/employee/all_info");
        } else if (req.getParameter("Back") != null) {
            resp.sendRedirect(req.getContextPath() + "/PBook/employee/all_info");
        }
        logger.info("[WEB] EmployeeController method employeeDoCreate end");
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView employeeDoDelete(@RequestParam("id") int id){
        logger.info("[WEB] EmployeeController method employeeDoDelete start");
        logger.debug("Remote employee id{} " + id);
        employeeService.delete(id);
        logger.info("[WEB] EmployeeController method employeeDoDelete end");

        return new ModelAndView("redirect:all_info");
    }
}

