package com.getjavajob.nesterenkom.pbook.servlet.filter;

import com.getjavajob.nesterenkom.pbook.model.User;
import com.getjavajob.nesterenkom.pbook.util.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AccessRightFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(AccessRightFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("AccessRightFilter method doFilter start");
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        User currentUser = (User) req.getSession().getAttribute("User");
        logger.debug("Current user " + currentUser);
        if (currentUser == null) {
            Cookie[] cookies = req.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("UserLogin")) {
                        currentUser = Users.getUser(cookie.getValue());
                        req.getSession().setAttribute("User", currentUser);

                    }
                }
            }

            if (currentUser == null){
                resp.sendRedirect("/PBook/login");
                return;
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
        logger.info("AccessRightFilter method doFilter end");
    }

    @Override
    public void destroy() {

    }
}
