package com.getjavajob.nesterenkom.pbook.servlet.controller;

import com.getjavajob.nesterenkom.pbook.model.Department;
import com.getjavajob.nesterenkom.pbook.model.Employee;
import com.getjavajob.nesterenkom.pbook.service.DepartmentService;
import com.getjavajob.nesterenkom.pbook.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("department")
public class DepartmentController {

    private static final Logger logger = LoggerFactory.getLogger(DepartmentController.class);

    @Autowired
    private DepartmentService<Department> departmentService;

    @Autowired
    private EmployeeService<Employee> employeeService;

    @RequestMapping(value = "/all_info", method = RequestMethod.GET)
    public String departmentsInfo(Model model) {
        logger.info("[WEB] DepartmentController method departmentsInfo start");
        List<Department> departments = departmentService.getAll();
        logger.info("List departments " + departments);
        model.addAttribute("departments", departments);
        logger.info("[WEB] DepartmentController method departmentsInfo end");

        return "departments_table";
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    public String departmentInfo(@RequestParam(value = "id") int id, Model model) {
        logger.info("[WEB] DepartmentController method departmentInfo start");
        Department department = departmentService.get(id);
        logger.info("Get information department " + department);
        model.addAttribute("department", department);
        logger.info("[WEB] DepartmentController method departmentInfo end");

        return "department_info";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView departmentUpdate(@RequestParam(value = "id") int id) {
        logger.info("[WEB] DepartmentController method departmentUpdate start");
        Department department = departmentService.get(id);
        logger.info("Update information department " + department);
        ModelAndView model = new ModelAndView("department_update");
        model.addObject("department", department);
        model.addObject("allEmployees", employeeService.getAll());
        logger.info("Information Department before changes " + department);
        logger.info("[WEB] DepartmentController method departmentUpdate end");

        return model;
    }

    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST)
    public void departmentDoUpdate(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.info("[WEB] DepartmentController method departmentDoUpdate start");
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");

        Department department = departmentService.get(Integer.valueOf(req.getParameter("id")));
        department.setName("name");
        String headDepartmentId = req.getParameter("headDepartmentId");
        department.setHeadDepartment(employeeService.get(Integer.parseInt(headDepartmentId)));
        logger.info("Information Department after changes " + department);

        if (req.getParameter("Save") != null) {
            departmentService.update(department);
            resp.sendRedirect(req.getContextPath() + "/PBook/department/all_info");
            logger.info("[WEB] DepartmentController method departmentDoUpdate end");
        }
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView departmentCreate(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        logger.info("[WEB] DepartmentController method departmentCreate start");
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");

        ModelAndView model = new ModelAndView("department_create");
        model.addObject("allEmployees", employeeService.getAll());
        logger.info("[WEB] DepartmentController method departmentCreate end");

        return model;
    }

    @RequestMapping(value = "/doCreate", method = RequestMethod.POST)
    public void departmentDoCreate(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        logger.info("[WEB] DepartmentController method departmentDoCreate start");
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");

        Department department = new Department();
        department.setName(req.getParameter("name"));
        String headDepartmentId = req.getParameter("headDepartmentId");
        department.setHeadDepartment(employeeService.get(Integer.parseInt(headDepartmentId)));
        logger.info("New department " + department);

        if (req.getParameter("Save") != null) {
            departmentService.add(department);
            resp.sendRedirect(req.getContextPath() + "/PBook/department/all_info");
            logger.info("[WEB] DepartmentController method departmentDoCreate end");
        }
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView departmentDoDelete(@RequestParam("id") int id) {
        logger.info("[WEB] DepartmentController method departmentDoDelete start");
        logger.info("Remote department id{}= " + id);
        departmentService.delete(id);
        logger.info("[WEB] DepartmentController method departmentDoDelete end");

        return new ModelAndView("redirect:all_info");
    }
}

