package com.getjavajob.nesterenkom.pbook.servlet.controller;

import com.getjavajob.nesterenkom.pbook.model.Employee;
import com.getjavajob.nesterenkom.pbook.service.EmployeeService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AjaxEmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(AjaxEmployeeController.class);

    @Autowired
    private EmployeeService<Employee> employeeService;

    @RequestMapping(value = "getEmployees")
    public List<Employee> getEmployees(final @RequestParam("filter") String filter) {
        logger.info("[WEB] AjaxEmployeeController method getEmployees start");
        List<Employee> employees = employeeService.getAll();
        CollectionUtils.filter(employees, new Predicate<Employee>() {
            @Override
            public boolean evaluate(Employee employee) {
                logger.debug("The employee %s %s %s is searched = ", employee.getSurname(), employee.getName(), employee.getMiddleName());
                return employee.getSurname().contains(filter) || employee.getName().contains(filter) || employee.getMiddleName().contains(filter);
            }
        });
        logger.info("[WEB] AjaxEmployeeController method getEmployees end");
        return employees;
    }
}
