<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.getjavajob.nesterenkom.pbook</groupId>
    <artifactId>pbook</artifactId>
    <packaging>pom</packaging>
    <version>0.0.1-SNAPSHOT</version>

    <modules>
        <module>common</module>
        <module>dao</module>
        <module>service</module>
        <module>webapp</module>
    </modules>

    <properties>
        <java.version>1.7</java.version>
        <encoding>UTF-8</encoding>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <javaee-api.version>7.0</javaee-api.version>
        <junit.version>4.11</junit.version>
        <hamcrest.version>1.3.RC2</hamcrest.version>
        <dbunit.version>2.5.1</dbunit.version>
        <mockito.version>1.10.19</mockito.version>
        <maven-compiler-plugin.version>3.3</maven-compiler-plugin.version>
        <maven-sourse-plugin.version>2.4</maven-sourse-plugin.version>
        <mysql.version>5.1.37</mysql.version>
        <h2.version>1.4.190</h2.version>
        <servlet-api.version>3.1.0</servlet-api.version>
        <jstl.version>1.2</jstl.version>
        <animate-css.version>3.3.0</animate-css.version>
        <spring.version>4.2.4.RELEASE</spring.version>
        <hibernate.version>5.0.5.Final</hibernate.version>
        <jackson.version>2.7.2</jackson.version>
        <jackson_mapper.version>1.9.13</jackson_mapper.version>
        <apache_collections4.version>4.1</apache_collections4.version>
        <apache_commons_io.version>1.3.2</apache_commons_io.version>
        <apache_commons_codec.version>1.10</apache_commons_codec.version>
        <apache_commons.version>2.0.1</apache_commons.version>

        <log4j2.version>2.5</log4j2.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>javax</groupId>
                <artifactId>javaee-api</artifactId>
                <version>${javaee-api.version}</version>
            </dependency>
            <!-- TEST DEPENDENCIES-->
            <!--Test junit-->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.dbunit</groupId>
                <artifactId>dbunit</artifactId>
                <version>${dbunit.version}</version>
            </dependency>
            <!--Test hamcrest-->
            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-library</artifactId>
                <version>${hamcrest.version}</version>
                <scope>test</scope>
            </dependency>
            <!-- Test mockito-->
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-all</artifactId>
                <version>${mockito.version}</version>
                <scope>test</scope>
            </dependency>
            <!--H2 DB-->
            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>${h2.version}</version>
                <scope>test</scope>
            </dependency>

            <!--LAYER DEPENDENCIES-->
            <!--Common layer-->
            <dependency>
                <groupId>com.getjavajob.nesterenkom.pbook</groupId>
                <artifactId>common</artifactId>
                <version>${project.version}</version>
            </dependency>
            <!--Dao layer-->
            <dependency>
                <groupId>com.getjavajob.nesterenkom.pbook</groupId>
                <artifactId>dao</artifactId>
                <version>${project.version}</version>
            </dependency>
            <!-- Service layer-->
            <dependency>
                <groupId>com.getjavajob.nesterenkom.pbook</groupId>
                <artifactId>service</artifactId>
                <version>${project.version}</version>
            </dependency>
            <!--LAYER DEPENDENCIES END-->

            <!--DAO LAYER DEPENDENCIES-->
            <!--MySQL DB-->
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql.version}</version>
            </dependency>
            <!--DAO LAYER DEPENDENCIES END-->

            <!--WEB LAYER DEPENDENCIES-->
            <!-- Servlet API-->
            <dependency>
                <groupId>javax.servlet</groupId>
                <artifactId>javax.servlet-api</artifactId>
                <version>${servlet-api.version}</version>
                <scope>provided</scope>
            </dependency>
            <!-- JSTL-->
            <dependency>
                <groupId>jstl</groupId>
                <artifactId>jstl</artifactId>
                <version>${jstl.version}</version>
                <scope>provided</scope>
            </dependency>
            <!--Animate css-->
            <dependency>
                <groupId>org.webjars</groupId>
                <artifactId>animate.css</artifactId>
                <version>${animate-css.version}</version>
            </dependency>
            <!--WEB LAYER DEPENDENCIES END-->

            <!--SPRING-->
            <!--spring context-->
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-core</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context-support</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jdbc</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-orm</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-tx</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-web</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-webmvc</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-test</artifactId>
                <version>${spring.version}</version>
            </dependency>
            <!--SPRING End-->

            <!--HIBERNATE-->
            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-core</artifactId>
                <version>${hibernate.version}</version>
            </dependency>
            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-entitymanager</artifactId>
                <version>${hibernate.version}</version>
            </dependency>
            <!--HIBERNATE End-->

            <!--JACKSON-->
            <!-- Need this for json to/from object -->
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-core</artifactId>
                <version>${jackson.version}</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson.version}</version>
            </dependency>
            <dependency>
                <groupId>org.codehaus.jackson</groupId>
                <artifactId>jackson-mapper-asl</artifactId>
                <version>${jackson_mapper.version}</version>
            </dependency>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-annotations</artifactId>
                <version>${jackson.version}</version>
            </dependency>
            <!--JACKSON End-->

            <!--Apache commons-->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-dbcp2</artifactId>
                <version>${apache_commons.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-io</artifactId>
                <version>${apache_commons_io.version}</version>
            </dependency>
            <dependency>
                <groupId>commons-codec</groupId>
                <artifactId>commons-codec</artifactId>
                <version>${apache_commons_codec.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-collections4</artifactId>
                <version>${apache_collections4.version}</version>
            </dependency>
            <!--Apache commons End-->

            <!--Logging-->
            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-api</artifactId>
                <version>${log4j2.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-core</artifactId>
                <version>${log4j2.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-slf4j-impl</artifactId>
                <version>${log4j2.version}</version>
            </dependency>
            <!--Logging End-->
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                    <encoding>${encoding}</encoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>${maven-sourse-plugin.version}</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>