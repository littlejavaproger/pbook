package com.getjavajob.nesterenkom.pbook.service.impl;

import com.getjavajob.nesterenkom.pbook.model.Employee;
import com.getjavajob.nesterenkom.pbook.model.Telephone;
import com.getjavajob.nesterenkom.pbook.model.TypeTelephone;
import com.getjavajob.nesterenkom.pbook.repository.EmployeeRepository;
import com.getjavajob.nesterenkom.pbook.repository.TelephoneRepository;
import com.getjavajob.nesterenkom.pbook.service.TelephoneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class TelephoneServiceImpl implements TelephoneService<Telephone> {

    private static final Logger logger = LoggerFactory.getLogger(TelephoneServiceImpl.class);

    @Autowired
    private TelephoneRepository<Telephone> telephoneRepository;

    @Autowired
    private EmployeeRepository<Employee> employeeRepository;

    @Transactional
    @Override
    public Telephone add(Telephone telephone) {
        logger.info("[SERVICE] Telephone method add start");
        logger.debug("Service layer method=[add] telephone [ %s ]", telephone);

        if (telephone.getNumber() == null || telephone.getType() == null){
            logger.error("Service layer method=[add] telephone number %s is null or type %s is null", telephone.getNumber(), telephone.getType());
        }
        logger.info("[SERVICE] Telephone method add end");

        return telephoneRepository.add(telephone);
    }

    @Transactional
    @Override
    public Telephone update(Telephone telephone) {
        logger.info("[SERVICE] Telephone method update start");
        logger.debug("Service layer method=[update] telephone [ %s ]", telephone);

        if (telephone.getNumber() == null || telephone.getType() == null){
            logger.error("Service layer method=[update] telephone number %s is null or type %s is null", telephone.getNumber(), telephone.getType());
        }
        logger.info("[SERVICE] Telephone method update end");

        return telephoneRepository.update(telephone);
    }

    @Transactional
    @Override
    public void delete(int id) {
        logger.info("[SERVICE] Telephone method delete start");
        logger.debug("Service layer method=[delete] telephone id={%s}", id);

        if (id < 0){
            logger.error("Service layer method=[delete] telephone id={%s} not found", id);
        }
        logger.info("[SERVICE] Telephone method delete end");

        telephoneRepository.delete(id);
    }

    @Override
    public Telephone get(int id) {
        logger.info("[SERVICE] Telephone method get start");
        logger.debug("Service layer method=[find by id] telephone id={%s}", id);

        if (id < 0){
            logger.error("Service layer method=[find by id] telephone id={%s} not found", id);
        }
        logger.info("[SERVICE] Telephone method get end");

        return telephoneRepository.findById(id);
    }

    @Override
    public List<Telephone> getAll() {
        logger.info("[SERVICE] Telephone method getAll start");
        List<Telephone> telephones = telephoneRepository.findAll();
        logger.debug("Service layer method=[find all] list telephones %s", telephones);
        logger.info("[SERVICE] Telephone method getAll end");

        return telephones;
    }

    @Override
    public List<TypeTelephone> getAllTypeTelephone() {
        logger.info("[SERVICE] Telephone method getAllTypeTelephone start");
        List<TypeTelephone> typeTelephones = new ArrayList<>();
        typeTelephones.add(TypeTelephone.HOME);
        typeTelephones.add(TypeTelephone.WORK);
        typeTelephones.add(TypeTelephone.DEPARTMENT);
        typeTelephones.add(TypeTelephone.MOBILE);
        logger.info("[SERVICE] Telephone method getAllTypeTelephone end");

        return typeTelephones;
    }

    @Override
    public void addTelephoneNumber(Employee employee, Telephone telephone) {
        logger.info("[SERVICE] Telephone method addTelephoneNumber start");
        employee.getTelephones().add(telephone);
        employeeRepository.update(employee);
        logger.info("[SERVICE] Telephone method addTelephoneNumber end");
    }

    @Override
    public void deleteTelephoneNumber(Employee employee, Telephone telephone) {
        logger.info("[SERVICE] Telephone method deleteTelephoneNumber start");
        employee.getTelephones().remove(telephone);
        employeeRepository.update(employee);
        logger.info("[SERVICE] Telephone method deleteTelephoneNumber end");
    }
}
