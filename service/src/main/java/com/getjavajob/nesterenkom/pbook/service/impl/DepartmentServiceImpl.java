package com.getjavajob.nesterenkom.pbook.service.impl;

import com.getjavajob.nesterenkom.pbook.model.Department;
import com.getjavajob.nesterenkom.pbook.repository.DepartmentRepository;
import com.getjavajob.nesterenkom.pbook.service.DepartmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class DepartmentServiceImpl implements DepartmentService<Department> {

    private static final Logger logger = LoggerFactory.getLogger(DepartmentServiceImpl.class);

    @Autowired
    private DepartmentRepository<Department> departmentRepository;

    @Transactional
    @Override
    public Department add(Department department) {
        logger.info("[SERVICE] Department method add start");
        logger.debug("Service layer method=[add] department [ %s ]", department);

        if (department.getName() == null){
            logger.error("Service layer method=[add] department name %s is null", department.getName());
        }else if(department.getHeadDepartment() == null){
            logger.error("Service layer method=[add] department header %s is null", department.getHeadDepartment());
        }
        logger.info("[SERVICE] Department method add end");

        return departmentRepository.add(department);
    }

    @Transactional
    @Override
    public Department update(Department department) {
        logger.info("[SERVICE] Department method update start");
        logger.debug("Service layer method=[update] department [ %s ]", department);

        if (department.getName() == null){
            logger.error("Service layer method=[update] department name %s is null", department.getName());
        }else if(department.getHeadDepartment() == null){
            logger.error("Service layer method=[update] department header %s is null", department.getHeadDepartment());
        }
        logger.info("[SERVICE] Department method update end");

        return departmentRepository.update(department);
    }

    @Transactional
    @Override
    public void delete(int id) {
        logger.info("[SERVICE] Department method delete start");
        logger.debug("Service layer method=[delete] department id={%s}", id);

        if (id < 0){
            logger.error("Service layer method=[delete] department id={%s} not found", id);
        }
        logger.info("[SERVICE] Department method delete end");

        departmentRepository.delete(id);
    }

    @Override
    public Department get(int id) {
        logger.info("[SERVICE] Department method get start");
        logger.debug("Service layer method=[find by id] department id={%s}", id);

        if (id < 0) {
            logger.error("Service layer method=[find by id] department id={%s} not found", id);
        }
        logger.info("[SERVICE] Department method get start");

        return departmentRepository.findById(id);
    }

    @Override
    public List<Department> getAll() {
        logger.info("[SERVICE] Department method getAll start");
        List<Department> departments = departmentRepository.findAll();
        logger.debug("Service layer method=[find all] list addresses %s", departments);
        logger.info("[SERVICE] Department method getAll start");

        return departments;
    }
}
