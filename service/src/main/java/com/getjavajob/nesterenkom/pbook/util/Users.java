package com.getjavajob.nesterenkom.pbook.util;

import com.getjavajob.nesterenkom.pbook.model.ListRole;
import com.getjavajob.nesterenkom.pbook.model.User;

import java.util.HashMap;
import java.util.Map;

public class Users {
    private static Map<String, User> userSet;

    static {
        userSet = new HashMap<>();
        userSet.put("User", new User("User", "User", ListRole.USER));
        userSet.put("Admin", new User("Admin", "Admin", ListRole.ADMIN));
    }

    public static User getUser(String login, String password) {
        User user = userSet.get(login);

        if (user != null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }

    public static User getUser(String login) {
        return userSet.get(login);
    }
}
