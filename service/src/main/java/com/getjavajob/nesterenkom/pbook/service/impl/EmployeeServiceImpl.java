package com.getjavajob.nesterenkom.pbook.service.impl;

import com.getjavajob.nesterenkom.pbook.model.*;
import com.getjavajob.nesterenkom.pbook.repository.EmployeeRepository;
import com.getjavajob.nesterenkom.pbook.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class EmployeeServiceImpl implements EmployeeService<Employee> {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeRepository<Employee> employeeRepository;

    @Transactional
    @Override
    public Employee add(Employee employee) {
        logger.info("[SERVICE] Employee method add start");
        logger.debug("Service layer method=[add] employee [ %s ]", employee);

        if (employee.getSurname() == null || employee.getName() == null || employee.getMiddleName() == null){
            logger.error("Service layer method=[add] employee surname %s is null or name %s is null or middle name %s is null",
                    employee.getSurname(), employee.getName(), employee.getMiddleName());
        }else if(employee.getLeader().getSurname() == null || employee.getLeader().getName() == null || employee.getLeader().getMiddleName() == null){
            logger.error("Service layer method=[add] employee leader surname %s is null or name %s is null or middle name %s is null",
                    employee.getLeader().getSurname(), employee.getLeader().getName(), employee.getLeader().getMiddleName());
        }
        logger.info("[SERVICE] Employee method add end");

        return employeeRepository.add(employee);
    }

    @Transactional
    @Override
    public Employee update(Employee employee) {
        logger.info("[SERVICE] Employee method update start");
        logger.debug("Service layer method=[update] employee [ %s ]", employee);

        if (employee.getSurname() == null || employee.getName() == null || employee.getMiddleName() == null){
            logger.error("Service layer method=[update] employee surname %s is null or name %s is null or middle name %s is null",
                    employee.getSurname(), employee.getName(), employee.getMiddleName());
        }else if(employee.getLeader().getSurname() == null || employee.getLeader().getName() == null || employee.getLeader().getMiddleName() == null){
            logger.error("Service layer method=[update] employee leader surname %s is null or name %s is null or middle name %s is null",
                    employee.getLeader().getSurname(), employee.getLeader().getName(), employee.getLeader().getMiddleName());
        }
        logger.info("[SERVICE] Employee method update end");

        return employeeRepository.update(employee);
    }

    @Transactional
    @Override
    public void delete(int id) {
        logger.info("[SERVICE] Employee method delete start");
        logger.debug("Service layer method=[delete] employee id={%s}", id);

        if (id < 0) {
            logger.error("Service layer method=[delete] employee id={%s} not found", id);
        }
        logger.info("[SERVICE] Employee method delete end");

        employeeRepository.delete(id);
    }

    @Override
    public Employee get(int id) {
        logger.info("[SERVICE] Employee method get start");
        logger.debug("Service layer method=[find by id] employee id={%s}", id);

        if (id < 0){
            logger.error("Service layer method=[find by id] employee id={%s} not found", id);
        }
        logger.info("[SERVICE] Employee method get end");

        return employeeRepository.findById(id);
    }

    @Override
    public List<Employee> getAll() {
        logger.info("[SERVICE] Employee method getAll start");
        List<Employee> employees = employeeRepository.findAll();
        logger.debug("Service layer method=[find all] list addresses %s", employees);
        logger.info("[SERVICE] Employee method getAll end");

        return employees;
    }
}
