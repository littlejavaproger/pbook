package com.getjavajob.nesterenkom.pbook.service.impl;

import com.getjavajob.nesterenkom.pbook.model.Address;
import com.getjavajob.nesterenkom.pbook.model.Employee;
import com.getjavajob.nesterenkom.pbook.model.TypeAddress;
import com.getjavajob.nesterenkom.pbook.repository.AddressRepository;
import com.getjavajob.nesterenkom.pbook.repository.EmployeeRepository;
import com.getjavajob.nesterenkom.pbook.service.AddressService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class AddressServiceImpl implements AddressService<Address> {

    private static final Logger logger = LoggerFactory.getLogger(AddressServiceImpl.class);

    @Autowired
    private AddressRepository<Address> addressRepository;

    @Autowired
    private EmployeeRepository<Employee> employeeRepository;

    @Transactional
    @Override
    public Address add(Address address) {
        logger.info("[SERVICE] Address method add start");
        logger.debug("Service layer method=[add] address [ %s ]", address);

        if (address.getAddress() == null || address.getType() == null){
            logger.error("Service layer method=[add] address %s is null or type %s is null", address.getAddress(), address.getType());
        }
        logger.info("[SERVICE] Address method add end");

        return addressRepository.add(address);
    }

    @Transactional
    @Override
    public Address update(Address address) {
        logger.info("[SERVICE] Address method update start");
        logger.debug("Service layer method=[update] address [ %s ]", address);

        if (address.getAddress() == null || address.getType() == null){
            logger.error("Service layer method=[update] address %s is null or type %s is null", address.getAddress(), address.getType());
        }
        logger.info("[SERVICE] Address method update end");

        return addressRepository.update(address);
    }

    @Transactional
    @Override
    public void delete(int id) {
        logger.info("[SERVICE] Address method delete start");
        logger.debug("Service layer method=[delete] address id={%s}", id);

        if (id < 0){
            logger.error("Service layer method=[delete] address id={%s} not found", id);
        }
        logger.info("[SERVICE] Address method delete end");

        addressRepository.delete(id);
    }

    @Override
    public Address get(int id) {
        logger.info("[SERVICE] Address method get start");
        logger.debug("Service layer method=[find by id] address id={%s}", id);

        if (id < 0){
            logger.error("Service layer method=[find by id] address id={%s} not found", id);
        }
        logger.info("[SERVICE] Address method get end");

        return addressRepository.findById(id);
    }

    @Override
    public List<Address> getAll() {
        logger.info("[SERVICE] Address method getAll start");
        List<Address> addresses = addressRepository.findAll();
        logger.debug("Service layer method=[find all] list addresses %s", addresses);
        logger.info("[SERVICE] Address method getAll end");

        return addresses;
    }

    @Override
    public List<TypeAddress> getAllTypeAddress() {
        logger.info("[SERVICE] Address method getAllTypeAddress start");
        List<TypeAddress> typeAddresses = new ArrayList<>();
        typeAddresses.add(TypeAddress.HOME);
        typeAddresses.add(TypeAddress.WORK);
        logger.info("[SERVICE] Address method getAllTypeAddress end");

        return typeAddresses;
    }

    @Override
    public void addAddress(Employee employee, Address address) {
        logger.info("[SERVICE] Address method addAddress start");
        employee.getAddresses().add(address);
        logger.info("[SERVICE] Address method addAddress end");

        employeeRepository.update(employee);
    }
}
