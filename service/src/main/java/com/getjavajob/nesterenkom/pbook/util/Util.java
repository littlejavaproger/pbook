package com.getjavajob.nesterenkom.pbook.util;

import com.getjavajob.nesterenkom.pbook.model.Telephone;
import com.getjavajob.nesterenkom.pbook.model.TypeAddress;
import com.getjavajob.nesterenkom.pbook.model.TypeTelephone;
import org.apache.commons.io.IOUtils;

import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Util {
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public static Map<TypeTelephone, List<Telephone>> splitPhones(List<Telephone> telephones) {
        List<Telephone> workPhones = new ArrayList<>();
        List<Telephone> privatePhones = new ArrayList<>();
        Map<TypeTelephone, List<Telephone>> result = new HashMap<>();

        for (Telephone telephone : telephones) {
            switch (telephone.getType()) {
                case HOME:
                    privatePhones.add(telephone);
                    break;
                case WORK:
                    workPhones.add(telephone);
                    break;
            }
        }
        result.put(TypeTelephone.HOME, privatePhones);
        result.put(TypeTelephone.WORK, workPhones);
        return result;
    }

    public static Date convertStringToDate(String date) {
        Date birthDate = null;
        try {
            birthDate = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            System.err.println("Parse Date " + "\"" + date + "\"" + "is incorrect");
        }
        return birthDate;
    }

    public static String convertDateToString(Date date) {
        String birthDate;
            birthDate = simpleDateFormat.format(date);
        return birthDate;
    }

    public static TypeTelephone convertStringToTypeTelephone(String type) {
        if (TypeTelephone.HOME.name().equals(type)) {
            return TypeTelephone.HOME;
        } else if (TypeTelephone.WORK.name().equals(type)) {
            return TypeTelephone.WORK;
        } else if(TypeTelephone.MOBILE.name().equals(type)) {
            return TypeTelephone.MOBILE;
        } else if (TypeTelephone.DEPARTMENT.name().equals(type)){
            return TypeTelephone.DEPARTMENT;
        }
        return null;
    }

    public static TypeAddress convertStringToTypeAddress(String type) {
        if (TypeAddress.HOME.name().equals(type)) {
            return TypeAddress.HOME;
        } else if (TypeAddress.WORK.name().equals(type)) {
            return TypeAddress.WORK;
        }
        return null;
    }
    public static String convertBlobToString(Blob blob) {
        String base64String = null;
        try {
            if (blob != null){
                try {
                    InputStream is = blob.getBinaryStream();
                    byte[] bytes = IOUtils.toByteArray(is);
                    base64String = new String(bytes, "UTF-8");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return base64String;
    }

    public static Blob convertStringToBlob(String base64String){
        Blob blob = null;
        if (base64String != null){
            try {
                byte[] bytes = base64String.getBytes();
                blob = new SerialBlob(bytes);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return blob;
    }
}
