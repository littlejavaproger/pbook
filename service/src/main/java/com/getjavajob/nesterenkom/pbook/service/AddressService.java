package com.getjavajob.nesterenkom.pbook.service;

import com.getjavajob.nesterenkom.pbook.model.*;

import java.util.List;

public interface AddressService<T> extends CrudService<T> {

    List<TypeAddress> getAllTypeAddress();

    void addAddress(Employee employee, Address address);
}
