package com.getjavajob.nesterenkom.pbook.service;

import com.getjavajob.nesterenkom.pbook.model.Employee;
import com.getjavajob.nesterenkom.pbook.model.Telephone;
import com.getjavajob.nesterenkom.pbook.model.TypeTelephone;

import java.util.List;

public interface TelephoneService<T> extends CrudService<T> {

    List<TypeTelephone> getAllTypeTelephone();

    void addTelephoneNumber(Employee employee, Telephone telephone);

    void deleteTelephoneNumber(Employee employee, Telephone telephone);
}
