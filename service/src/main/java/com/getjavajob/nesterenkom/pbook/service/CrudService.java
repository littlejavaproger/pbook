package com.getjavajob.nesterenkom.pbook.service;

import java.util.List;

/**
 * Created by apple on 24.12.15.
 */
public interface CrudService<T> {
    T add(T entity);
    T update(T entity);
    void delete(int id);
    T get(int id);
    List<T> getAll();
}
