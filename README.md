# **PBook (PhoneBook)** #
#### Telephone book for organizations, which allows you to store data about employees and the organizations
---
#### **Functionality:** ####
* create/edit/delete employee
* create/edit/delete department
* add boss to employee
* add head to department
* add/delete phone input fields dynamically
* JS helpers, notifications
* upload, instantly show avatar
* ajax search
* JS/JQuery animation effects

#### **Tools: ** ####
JDK 7, Spring 4, JPA 2 / Hibernate 4, MySql, H2Database, JUnit 4, JSP/JSTL, Log4j, Maven 3, jQuery 1, Twitter Bootstrap 3, Animate CSS, Jackson 2, Git / Bitbucket, Tomcat 7,  IntelliJIDEA 15.

---
#### **Nesterenko Maxim ** ####
Тренинг getJavaJob,  
http://www.getjavajob.com
![logo.jpg](https://bitbucket.org/repo/xqe9rE/images/1424713789-logo.jpg)
![all emloyee.png](https://bitbucket.org/repo/xqe9rE/images/3216232252-all%20emloyee.png)
![edit employee.png](https://bitbucket.org/repo/xqe9rE/images/340408933-edit%20employee.png)
![info employee.png](https://bitbucket.org/repo/xqe9rE/images/2429026966-info%20employee.png)